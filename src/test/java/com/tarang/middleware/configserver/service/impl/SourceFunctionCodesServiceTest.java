package com.tarang.middleware.configserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.RETURNS_DEFAULTS;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.when;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.tarang.middleware.configserver.ConfigServerApiApplication;
import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;
import com.tarang.middleware.configserver.entity.SourceFunctionCodeEntity;
import com.tarang.middleware.configserver.repository.SourceFunctionCodeRepository;
import com.tarang.middleware.configserver.service.SourceFunctionCodeService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConfigServerApiApplication.class)
public class SourceFunctionCodesServiceTest {
	
	@Autowired
	private SourceFunctionCodeService service;
	 
	@MockBean
	private SourceFunctionCodeRepository repository;
	
	
	private SourceFunctionCodeDto dto;
	
	
	private SourceFunctionCodeEntity entity;

	
	@org.junit.Before
	public void init() {

		ModelMapper modelMapper = new ModelMapper();
		dto  = new SourceFunctionCodeDto();
		entity =modelMapper.map(dto,SourceFunctionCodeEntity.class);

	}
	
//	@Test
//	public void getBySourceId() {
//		
//		List<SourceFunctionCodesEntity>   entityList =  new ArrayList<>();
//		entityList.add(entity);
//		
//       when(this.repository.findBySourceIdAndLanguageCode(1,"en")).thenReturn(entityList); 		
//       assertEquals(1, this.service.getBySourceIdAndLanguageCode(1, "en").size());
//		
//	}
	
	@Test
	public void addSourceFunctionDetails() {
		when(this.repository.save(entity)).thenReturn(entity);
		
		assertEquals(dto, this.service.addSourceFunctionCodes(dto));	
	}
	
	@Test
	public void getBySourceIdDetails() {
		
		when(this.repository.findBySourceId("1")).thenReturn(entity);
	
		assertEquals(1,this.service.getBysourceId("1").getSourceId());
			
	}
	@Test
	public void updateSourceFunctionCodesDetails() {
		when(this.repository.findBySourceId("1")).thenReturn(entity);
		when(this.repository.save(entity)).thenReturn(entity);
		assertEquals(entity.getSourceId(),this.service.getBysourceId("1").getSourceId());
        assertEquals(dto,this.service.updateSourceFunctionCodes(dto));
		
	}
	
	
}
