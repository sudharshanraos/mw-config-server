package com.tarang.middleware.configserver.controller;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.tarang.middleware.configserver.ConfigServerApiApplication;
import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;
import com.tarang.middleware.configserver.service.SourceFunctionCodeService;
import com.tarang.middleware.configserver.utils.TestModelDataDrive;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConfigServerApiApplication.class)
public class SourceFunctionCodesControllerTest {
	
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SourceFunctionCodesControllerTest.class);
	
    private MockMvc mockMvc;
    
    
    @Autowired
    private WebApplicationContext wac;
    
    
	@MockBean
	private SourceFunctionCodeService  sourceFunctionCodesService;
	
	
	
	@org.junit.Before
	public void setUp(){
		 this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
		
	}
	@Test	
	public void testAddSourceFunctionCodes() throws Exception{
			Mockito.when(sourceFunctionCodesService.addSourceFunctionCodes(Mockito.any())).thenReturn(new SourceFunctionCodeDto());
			String request=TestModelDataDrive.convertObjectToJson(new SourceFunctionCodeDto ());
			mockMvc.perform(MockMvcRequestBuilders.post(ApiConstants.SOURCE_FUNCTION_CODE_PARENT +ApiConstants.SOURCE_FUNCTION_CODE_ADD_CONFIG)
			.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(request)).andExpect(status().is2xxSuccessful());	
		}
	
	
	
	@Test
	public void testupdateSourceFunctionCodes() throws Exception {
		Mockito.when(sourceFunctionCodesService.updateSourceFunctionCodes(Mockito.any())).thenReturn(new SourceFunctionCodeDto());
		Mockito.when(sourceFunctionCodesService.getBysourceId("1")).thenReturn(new SourceFunctionCodeDto());
		String request=TestModelDataDrive.convertObjectToJson(new SourceFunctionCodeDto());
		mockMvc.perform(MockMvcRequestBuilders.post(ApiConstants.SOURCE_FUNCTION_CODE_PARENT + ApiConstants.UPDATE_SOURCE_FUNCTION_CODE+"?sourceId=1")
		.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(request)).andExpect(status().is2xxSuccessful());
	}
	
	@Test
	public void testGetSourceFunctionCodes() throws Exception{
		 List<SourceFunctionCodeDto> h = new ArrayList<>();
		 Mockito.when(sourceFunctionCodesService.getSourceFunctionCodesList()).thenReturn(h);		
		 mockMvc.perform(MockMvcRequestBuilders
                  .get(ApiConstants.SOURCE_FUNCTION_CODE_PARENT + ApiConstants.SOURCE_FUNCTION_CODE_LIST)
                  .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	
	
	
}




