package com.tarang.middleware.configserver.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.tarang.middleware.configserver.ConfigServerApiApplication;
import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.dto.SourceResponseDto;
import com.tarang.middleware.configserver.service.SourceResponseService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.TestModelDataDrive;






@RunWith(SpringRunner.class)
@SpringBootTest(classes = ConfigServerApiApplication.class)
public class SourceResponsesControllerTest {
	
	
	private static final Logger LOGGER=LoggerFactory.getLogger(SourceResponsesControllerTest.class);
	
	private MockMvc mockMvc;
	
	 @Autowired
	    private WebApplicationContext wac;
	 
    @MockBean
	private SourceResponseService sourceResponseService;
	
	
	
	@Before
	public void setup() {
		 this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

	}
	@Test
		public void testAddSourceResponse() throws Exception {
	    Mockito.when(sourceResponseService.addSourceResponse(Mockito.any())).thenReturn(new SourceResponseDto());
		String request = TestModelDataDrive.convertObjectToJson(new SourceResponseDto());
		mockMvc.perform(MockMvcRequestBuilders.post(ApiConstants.SOURCE_RESPONSE_PARENT + ApiConstants.SOURCE_RESPONSE_ADD_CONFIG)
				.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE)
				.content(request)).andExpect(status().is2xxSuccessful());
		
	}
	    @Test
		public void testUpdateSourceResponse() throws Exception {
			Mockito.when(sourceResponseService.updateSourceResponse(Mockito.any())).thenReturn(new SourceResponseDto());
			Mockito.when(sourceResponseService.getSourceById("1")).thenReturn(new SourceResponseDto());
			String request = TestModelDataDrive.convertObjectToJson(new SourceResponseDto());
			mockMvc.perform(MockMvcRequestBuilders.patch(ApiConstants.SOURCE_RESPONSE_PARENT + ApiConstants.UPDATE_SOURCE_RESPONSE +"?sourceId=1")
					.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE).content(request))
			        .andExpect(status().is2xxSuccessful());
		}
	    @Test
	    public void testGetSourceResponseList() throws Exception {
	    	List<SourceResponseDto> dto =new ArrayList<SourceResponseDto>();
	    	Mockito.when(sourceResponseService.getSourceResponseList()).thenReturn(dto);
	    	mockMvc.perform(MockMvcRequestBuilders.get(ApiConstants.SOURCE_RESPONSE_PARENT + ApiConstants.SOURCE_RESPONSE_LIST)
	    			.contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	    }
		
		
		

}
