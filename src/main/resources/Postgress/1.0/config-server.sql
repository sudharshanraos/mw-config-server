-- public.bin_range definition


CREATE TABLE bin_range (
	id int8 NOT NULL,
	bin varchar(255) NULL,
	description varchar(255) NULL,
	pan_length varchar(255) NULL,
	"range" varchar(255) NULL,
	CONSTRAINT bin_range_pkey PRIMARY KEY (id)
);


-- public.currency definition



CREATE TABLE currency (
	id int8 NOT NULL,
	code varchar(255) NULL,
	description varchar(255) NULL,
	exponent varchar(255) NULL,
	"name" varchar(255) NULL,
	CONSTRAINT currency_pkey PRIMARY KEY (id)
);


-- public.databasechangeloglock definition



CREATE TABLE databasechangeloglock (
	id int4 NOT NULL,
	"locked" bool NOT NULL,
	lockgranted timestamp NULL,
	lockedby varchar(255) NULL,
	CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id)
);


-- public.merchant_config definition



CREATE TABLE merchant_config (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_date timestamp NULL,
	updated_by varchar(255) NULL,
	updated_date timestamp NULL,
	merchant_id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	status varchar(255) NULL,
	terminal_id varchar(255) NOT NULL,
	CONSTRAINT merchant_config_pkey PRIMARY KEY (id),
	CONSTRAINT ukorvwxio53eygc03x3tka5309w UNIQUE (merchant_id)
);


-- public.message_config definition



CREATE TABLE message_config (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_date timestamp NULL,
	updated_by varchar(255) NULL,
	updated_date timestamp NULL,
	iso_request varchar(255) NULL,
	iso_response varchar(255) NULL,
	json_request varchar(255) NULL,
	json_response varchar(255) NULL,
	transaction_type varchar(20) NOT NULL,
	xml_request varchar(255) NULL,
	xml_response varchar(255) NULL,
	CONSTRAINT message_config_pkey PRIMARY KEY (id)
);


-- public.source_config definition



CREATE TABLE source_config (
	id int8 NOT NULL,
	request_timeout varchar(255) NULL,
	created_by varchar(255) NULL,
	created_date timestamp NULL,
	updated_by varchar(255) NULL,
	updated_date timestamp NULL,
	available_taragets varchar(255) NULL,
	connection_timeout varchar(255) NULL,
	connection_type varchar(255) NOT NULL,
	decrypt_key varchar(255) NULL,
	decrypt_type varchar(255) NULL,
	default_target varchar(255) NULL,
	encrypt_key varchar(255) NULL,
	encrypt_type varchar(255) NULL,
	headers varchar(255) NULL,
	iso_packager varchar(255) NULL,
	json_packager varchar(255) NULL,
	merchant_id varchar(255) NULL,
	message_format varchar(255) NOT NULL,
	port varchar(255) NULL,
	source_name varchar(255) NOT NULL,
	transaction_log varchar(255) NULL,
	uri varchar(255) NULL,
	validation varchar(255) NULL,
	validation_check varchar(255) NULL,
	xml_packager varchar(255) NULL,
	CONSTRAINT source_config_pkey PRIMARY KEY (id),
	CONSTRAINT uktned45n2deg7wflj0jx38xfft UNIQUE (source_name)
);


-- public.source_functioncodes definition



CREATE TABLE source_functioncodes (
	id int8 NOT NULL,
	source_id int8 NOT NULL,
	language_code varchar(10) NULL,
	function_code varchar(10) NULL,
	function_message varchar(500) NULL,
	created_by varchar(100) NULL,
	created_date timestamp NULL,
	updated_by varchar(100) NULL,
	updated_date timestamp NULL,
	CONSTRAINT source_functioncodes_pkey PRIMARY KEY (id)
);


-- public.source_message_mapping definition



CREATE TABLE source_message_mapping (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_date timestamp NULL,
	updated_by varchar(255) NULL,
	updated_date timestamp NULL,
	message_id int8 NOT NULL,
	source_id int8 NOT NULL,
	transaction_type varchar(255) NOT NULL,
	CONSTRAINT source_message_mapping_pkey PRIMARY KEY (id),
	CONSTRAINT uknftchcqiie034j0iqax9v3han UNIQUE (source_id, message_id, transaction_type)
);

-- public.source_responses definition



CREATE TABLE source_responses (
	id int8 NOT NULL,
	source_id int8 NOT NULL,
	language_code varchar(10) NULL,
	response_code varchar(10) NULL,
	response_message varchar(500) NULL,
	created_by varchar(100) NULL,
	created_date timestamp NULL,
	updated_by varchar(100) NULL,
	updated_date timestamp NULL,
	CONSTRAINT source_responses_pkey PRIMARY KEY (id)
);



-- public.target_config definition



CREATE TABLE target_config (
	id int8 NOT NULL,
	request_timeout varchar(255) NULL,
	created_by varchar(255) NULL,
	created_date timestamp NULL,
	updated_by varchar(255) NULL,
	updated_date timestamp NULL,
	connection_timeout varchar(255) NULL,
	connection_type varchar(255) NOT NULL,
	decrypt_key varchar(255) NULL,
	decrypt_type varchar(255) NULL,
	encrypt_key varchar(255) NULL,
	encrypt_type varchar(255) NULL,
	headers varchar(255) NULL,
	heartbeat varchar(255) NULL,
	heartbeat_delay varchar(255) NULL,
	iso_packager varchar(255) NULL,
	json_packager varchar(255) NULL,
	merchant_id varchar(255) NULL,
	message_format varchar(255) NOT NULL,
	port varchar(255) NULL,
	retry varchar(255) NULL,
	retry_count varchar(255) NULL,
	schema_type varchar(255) NULL,
	signin varchar(255) NULL,
	target_name varchar(255) NOT NULL,
	uri varchar(255) NULL,
	xml_packager varchar(255) NULL,
	CONSTRAINT target_config_pkey PRIMARY KEY (id),
	CONSTRAINT ukbpg0v0vcly6ye5txpg7vdu3ut UNIQUE (target_name)
);



-- public.target_functioncodes definition



CREATE TABLE target_functioncodes (
	id int8 NOT NULL,
	target_id int8 NOT NULL,
	language_code varchar(10) NULL,
	function_code varchar(10) NULL,
	function_message varchar(500) NULL,
	created_by varchar(100) NULL,
	created_date timestamp NULL,
	updated_by varchar(100) NULL,
	updated_date timestamp NULL,
	CONSTRAINT target_functioncodes_pkey PRIMARY KEY (id)
);


-- public.target_responses definition



CREATE TABLE target_responses (
	id int8 NOT NULL,
	target_id int8 NOT NULL,
	language_code varchar(10) NULL,
	response_code varchar(10) NULL,
	response_message varchar(500) NULL,
	created_by varchar(100) NULL,
	created_date timestamp NULL,
	updated_by varchar(100) NULL,
	updated_date timestamp NULL,
	CONSTRAINT target_responses_pkey PRIMARY KEY (id)
);