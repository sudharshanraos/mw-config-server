package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class SourceConfigDetailsDto
 * 
 * @author venkatramanap
 *
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SourceConfigDetailsDto implements Serializable{

	/** The constant serialVersionUID.*/
	private static final long serialVersionUID = 1L;

	/** The source function.*/
	private List<SourceFunctionCodeDto> sourceFunction;


	/** The source response.*/
	private List<SourceResponseDto> sourceResponse;


	/** The sourceConfig.*/
	private SourceConfigDto sourceConfig;


	/** The source message.*/
	private List<SourceMessageMappingDto> sourceMessage;



}
