package com.tarang.middleware.configserver.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class MessageConfigDto.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
public class MessageConfigDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Long id;
    
    private String created_by;
    
    private String updated_by;
    
    /** The iso request. */
    private String isoRequest;


    /** The iso response. */
    private String isoResponse;

    /** The json request. */
    private String jsonRequest;

    /** The json response. */
    private String jsonResponse;
    
    /** The transaction type. */
    private String transactionType;

    /** The xml request. */
    private String xmlRequest;

    /** The xml response. */
    private String xmlResponse;


}
