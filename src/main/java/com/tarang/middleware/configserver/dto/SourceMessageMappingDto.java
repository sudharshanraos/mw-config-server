package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



/**
 * The Class SourceMessageMappingDto
 * 
 * @author venkatramanap
 *
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SourceMessageMappingDto implements Serializable {
	
	/** The Constant serialVersionUID.*/
	private static final long serialVersionUID=1L;
	
	/** The id.*/
	private long id;
	
	/** The source id.*/
	private String sourceId;
	
	/** The message id.*/
	private long messageId;
	
	/** The transaction type.*/
	private String transactionType;
	
	/** The created by.*/
	private String createdBy;
	
	/** The created at.*/ 
	private Date createdAt;
	
	/** The updated by.*/
	private String updatedBy;
	
	/** The updated at.*/
	private Date updatedAt;
	

	
	
	


}
