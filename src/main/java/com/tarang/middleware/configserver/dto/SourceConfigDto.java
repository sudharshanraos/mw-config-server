package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class SourceConfigDto.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
public class SourceConfigDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Long id;
    
    /** The source id.*/
    private String sourceId;

    /** The source name. */
    private String sourceName;
    
    /** The port. */
    private String port;

    /** The merchant id. */
    private String merchantId;
    
    /** The merchant name.*/
    private String merchantName;
    
    /** The available targets. */
    private String availableTargets;

    /** The default target. */
    private String defaultTarget;
    
    /** The connection type. */
    private String connectionType;
    
    /** The uri. */
    private String uri;
    
    /** The connection timeout. */
    private String connectionTimeout;

    /** The Request timeout. */
    private String requestTimeout;
    
    /** The encrypt type. */
    private String encryptType;
    
    /** The encrypt key. */
    private String encryptKey;
    
    /** The decrypt type. */
    private String decryptType;
    
    /** The decrypt key. */
    private String decryptKey;
    
    /** The message format. */
    private String messageFormat;

    /** The headers. */
    private String headers;

    /** The iso packager.*/
    private String isoPackager;
    
    /** The json packager. */
    private String jsonPackager;

    /** The transaction log. */
    private String transactionLog;

    /** The validation. */
    private String validation;
 
    /** The validation check. */
    private String validationCheck;

    /** The created By.*/
    private String createdBy;
    
    /** The updated By.*/
    private String updatedBy;

    /** The xml packager. */
    private String xmlPackager;
    




    

    
 
    
  
    
}
