package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class SourceResponseDto.
 * 
 * @author venkatramanap.
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SourceResponseDto implements Serializable{
	
		 
    /** The Constant serialVersionUID.*/
	private static final long serialVersionUID = 1L;

     /** The id.*/
	 private int id;
	 
     /** The source id.*/	 
	 private String sourceId;
	 
	 /** The response code.*/ 
	 private String responseCode;
	 
	 /** The response message.*/	 	 
	 private String responseMessage;
	 
     /** The language code.*/	 
	 private String languageCode;
	 
     /** The created by.*/	 
	 private String createdBy;
	 
     /** The created at.*/
	 private Date createdAt;
	 
     /** The updated by.*/	 
	 private String updatedBy;
	 
     /** The updated at.*/	 
	 private Date updatedAt;
	 
	 
}
