package com.tarang.middleware.configserver.dto;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class SourceDetails {
    
   private SourceConfigDto sourceObj;
   
   
   private MessageConfigDto messageObj;

   

}
