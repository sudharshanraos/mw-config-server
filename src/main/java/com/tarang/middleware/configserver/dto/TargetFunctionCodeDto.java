package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The class TargetFunctionCodesDto
 * 
 * @author venkatramanap
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TargetFunctionCodeDto implements Serializable {
	
	/** The Constant serialVersionUID.*/
	public static final long serialVersionUID=1L;
	
	
	/** The id.*/
	private int id;
	
	
	/** The targetId.*/
	private String targetId;
	
	
	/** The languageCode.*/
	private String languageCode;
	
	
	/** The functionCode.*/
	private String functionCode;
	
	
	/** The functionMessage.*/
	private String functionMessage;
	
	
	/** The createdBy.*/
	private String createdBy;
	
	
	/** The createdAt.*/
	private Date createdAt;
	
	
	/** The updatedBy.*/
	private String updatedBy;
	
	
	/** The updatedAt.*/
	private Date updatedAt;
	

}
