package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The class TargetFunctionCodesDto
 * 
 * @author venkatramanap
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class UsersDto implements Serializable{
	
	
	/** The Constant serialVersionUID.*/
	private static final long serialVersionUID=1L;
	
	/* The id.*/
	private int id;
	
	/* The uuid.*/
	private String uuid;
	
	/* The login.*/
	private String login;
	
	/* The userName.*/
	private String userName;
	
	/* The email.*/
	private String email;
	
	/* The cryptedPassword.*/
	private String cryptedPassword;
	
	/* The hashMethod.*/
	private String hashMethod;
	
	/* The secretkey.*/
	private String secretKey;
	
	/* The active.*/
	private String active;
	
	/* The externalLogin.*/
	private String externalLogin;
	
	/* The externalId.*/
	private String externalId;
	
	/* The createdAt.*/
	private Date createdAt;
	
	/* The updatedAt.*/
	private Date updatedAt;
	
	/* The connectedAt.*/
	private Timestamp connectedAt;

}
