/*
 * 
 */
package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class TargetConfigDto.
 * 
 * @author sudharshan.s
 */

@Getter
@Setter
@NoArgsConstructor
public class TargetConfigDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	private long id;
	
	/** The target id.*/
    private String targetId;	

	/** The target name. */
	private String targetName;

	/** The merchant id. */
	private String merchantId;
	
	/** The merchant name.*/
	private String merchantName;
	
	/** The host. */
	private String host;
	
	/** The port. */
	private String port;
	
	/** The connection type. */
	private String connectionType;
	
	/** The uri. */
	private String uri;
	
	/** The connection timeout. */
	private String connectionTimeout;

	/** The Request timeout. */
	private String requestTimeout;
	
	/** The encrypt type. */
	private String encryptType;
	
	/** The encrypt key. */
	private String encryptKey;
	
	/** The decrypt type. */
	private String decryptType;
	
	/** The decrypt key. */
	private String decryptKey;

	/** The message format. */
	private String messageFormat;

	/** The headers. */
	private String headers;

	/** The iso packager. */
	private String isoPackager;

	/** The json packager.*/
	private String jsonPackager;
	
	/** The heartbeat. */
	private String heartbeat;

	/** The heartbeat dealy. */
	private String heartbeatDealy;
	
	/** The retry. */
	private String retry;

	/** The retry count. */
	private String retryCount;
	
	/** The schema type. */
	private String schemaType;
	
	/** The sign in. */
	private String signIn;
	
	/** The created by.*/
	private String createdBy;
	
	/** The created At.*/
	private Date createdAt;
	
	/** The  updated by.*/
	private String updatedBy;
	
	/** The  updated At.*/ 
	private Date updatedAt;

    /** The xml packager.*/
	private String xmlPackager;
	
	
	
	

}
