package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



/**
 * The class SourceFunctionCodesDto
 * @author venkatramanap
 *
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SourceFunctionCodeDto implements Serializable {
	
	/** The Constant serialVersionUID*/
	private static final long serialVersionUID=1L;
	
	
	/** The id.*/
	private int id;
	
	/** The sourceId.*/
	private String sourceId;
	
	/** The message id.*/
	private String messageId;
	
	/** The functionCode.*/
	private  String functionCode;
	
	/** The createdBy.*/
	private String createdBy;
	
	/** The createdAt.*/
	private Date createdAt;
	
	/** The updatedBy.*/
	private String updatedBy;
	
	/** The updated at.*/
	private Date updatedAt;
	
		
	
		

}
