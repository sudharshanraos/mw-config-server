package com.tarang.middleware.configserver.dto;

    import java.io.Serializable;
	import java.util.List;

	import lombok.AllArgsConstructor;
	import lombok.Getter;
	import lombok.NoArgsConstructor;
	import lombok.Setter;

	/**
	 * The Class TargetConfigDetailsDto.
	 * 
	 * @author venkatramanap.
	 *
	 */
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public class TargetConfigDetailsDto implements Serializable{
		
		/** The constant serialVersionUID.*/
		private static final long serialVersionUID=1L;
		
		
		/** The target config.*/
		private TargetConfigDto targetConfig;
		
		
		/** The target response.*/
		private List<TargetResponseDto> targetResponse;
		
		
		/** The target function codes.*/
		private List<TargetFunctionCodeDto> targetFunctionCodes;
		
		

	}

