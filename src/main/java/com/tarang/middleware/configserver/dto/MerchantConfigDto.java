package com.tarang.middleware.configserver.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The Class MerchantConfigDto.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@NoArgsConstructor
public class MerchantConfigDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    private Long id;

    /** The name. */
    private String merchantName;

    /** The merchant id. */
    private String merchantId;

    /** The terminal id. */
    private String terminalId;

    /** The status. */
    private String status;
    
    /** The additional date.*/
    private String additionalData;
    
    /** The createdby.*/
    private String createdBy;
    
    /** The updatedby.*/
    private String updatedBy;
    
    

}
