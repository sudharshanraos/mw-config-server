package com.tarang.middleware.configserver.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



/**
 * The Class TargetResponsesDto
 * 
 * @author venkatramanap
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TargetResponseDto implements Serializable {

	/** The Constant serialVersionUID.*/
	private static final long serialVersionUID=1L ;

	/** The id.*/	
	private int id;

	/** The targetid.*/	
	private String targetId;

	/** The responsecode.*/
	private String responseCode;

	/** The responsemessage.*/	
	private String responseMessage;
	
	/** The languagecode.*/	
	private String languageCode;
	
	/** The createdBy.*/	
	private String createdBy;
	
	/** The createdAt.*/
	private Date createdAt;

	/** The updatedBy.*/
	private String updatedBy;
	
	/** The updatedAt.*/
	private Date updatedAt;

}
