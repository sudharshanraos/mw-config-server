package com.tarang.middleware.configserver;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * The Class ConfigServerApiApplication.
 * 
 * @author sudharshan.s
 */
@EnableScheduling
@SpringBootApplication
public class ConfigServerApiApplication {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigServerApiApplication.class);

    /**
     * The main method.
     *
     * @param args
     *            the arguments
     */
    public static void main(String[] args) {
        LOGGER.info("ConfigServerApiApplication Starting..");
        SpringApplication.run(ConfigServerApiApplication.class, args);
    }
    
    @Bean 
	  public ModelMapper modelMapper() { 
		  return new ModelMapper(); 
	  }

}
