package com.tarang.middleware.configserver.utils;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.service.ConfigServerMessageService;

/**
 * The Class GeneralResponseCreator.
 * 
 * @author sudharshan.s
 */
@Component
public class GeneralResponseCreator {

    /** The message service. */
    private final ConfigServerMessageService messageService;

    /**
     * Instantiates a new general response creator.
     *
     * @param messageService
     *            the message service
     */
    public GeneralResponseCreator(ConfigServerMessageService messageService) {
        this.messageService = messageService;
    }

    /**
     * Creates the general response.
     *
     * @param <T>
     *            the generic type
     * @param code
     *            the code
     * @param isSuccess
     *            the is success
     * @param dataP
     *            the data P
     * @return the general response
     */
    public <T> GeneralResponse<T> createGeneralResponse(ConfigServerErrorCodes code, boolean isSuccess, T dataP) {
        GeneralResponse<T> generalResponse = new GeneralResponse<>();
        generalResponse.setCode(code.getCode());
        generalResponse.setMessage(messageService.getMessage(code.getDescription(), null));
        generalResponse.setSuccess(isSuccess);
        generalResponse.setData(dataP);
        return generalResponse;
    }

    /**
     * Creates the general response.
     *
     * @param <T>
     *            the generic type
     * @param code
     *            the code
     * @param params
     *            the params
     * @param isSuccess
     *            the is success
     * @param dataP
     *            the data P
     * @return the general response
     */
    public <T> GeneralResponse<T> createGeneralResponse(ConfigServerErrorCodes code, List<String> params, boolean isSuccess, T dataP) {
        String[] objects = !ObjectUtils.isEmpty(params) ? params.toArray(new String[0]) : null;
        GeneralResponse<T> generalResponse = new GeneralResponse<>();
        generalResponse.setCode(code.getCode());
        generalResponse.setMessage(messageService.getMessage(code.getDescription(), objects));
        generalResponse.setSuccess(isSuccess);
        generalResponse.setData(dataP);
        return generalResponse;
    }

    /**
     * Creates the general response for failure.
     *
     * @param <T>
     *            the generic type
     * @param code
     *            the code
     * @return the general response
     */
    public <T> GeneralResponse<T> createGeneralResponseForFailure(ConfigServerErrorCodes code) {
        GeneralResponse<T> generalResponse = new GeneralResponse<>();
        generalResponse.setCode(code.getCode());
        generalResponse.setMessage(messageService.getMessage(code.getDescription(), null));
        generalResponse.setSuccess(false);
        generalResponse.setData(null);
        return generalResponse;
    }

    /**
     * Creates the general response for failure.
     *
     * @param <T>
     *            the generic type
     * @param code
     *            the code
     * @param params
     *            the params
     * @return the general response
     */
    public <T> GeneralResponse<T> createGeneralResponseForFailure(ConfigServerErrorCodes code, List<String> params) {
        String[] objects = !ObjectUtils.isEmpty(params) ? params.toArray(new String[0]) : null;
        GeneralResponse<T> generalResponse = new GeneralResponse<>();
        generalResponse.setCode(code.getCode());
        generalResponse.setMessage(messageService.getMessage(code.getDescription(), objects));
        generalResponse.setSuccess(false);
        generalResponse.setData(null);
        return generalResponse;
    }

    /**
     * Creates the empty success response.
     *
     * @param <T>
     *            the generic type
     * @return the general response
     */
    public <T> GeneralResponse<T> createEmptySuccessResponse() {
        return createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, null);
    }

}
