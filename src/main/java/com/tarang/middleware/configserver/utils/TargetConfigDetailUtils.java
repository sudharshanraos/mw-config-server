package com.tarang.middleware.configserver.utils;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.tarang.middleware.configserver.dto.TargetConfigDetailsDto;
import com.tarang.middleware.configserver.dto.TargetConfigDto;
import com.tarang.middleware.configserver.dto.TargetFunctionCodeDto;
import com.tarang.middleware.configserver.dto.TargetResponseDto;
import com.tarang.middleware.configserver.entity.TargetConfigEntity;
import com.tarang.middleware.configserver.entity.TargetFunctionCodeEntity;
import com.tarang.middleware.configserver.entity.TargetResponseEntity;

public class TargetConfigDetailUtils {





	public static TargetConfigDetailsDto getTargetConfigDto(
			TargetConfigEntity config,List<TargetFunctionCodeEntity> functionCodesDetails,
			List<TargetResponseEntity> responseDetails)
	{
		ModelMapper modelMapper = new ModelMapper();
		List<TargetFunctionCodeDto> functionCodesDto = new ArrayList<>();
		List<TargetResponseDto>responseDto=new ArrayList<>();
		TargetConfigDetailsDto dto =new TargetConfigDetailsDto();
		for(TargetFunctionCodeEntity function : functionCodesDetails ) {
			TargetFunctionCodeDto codesDto =modelMapper.map(function,TargetFunctionCodeDto.class);
			functionCodesDto.add(codesDto);
		}

		for(TargetResponseEntity response : responseDetails) {
			TargetResponseDto targetResponses = modelMapper.map(response,TargetResponseDto.class);
			responseDto.add(targetResponses);
		}
		TargetConfigDto configDto = modelMapper.map(config,TargetConfigDto.class);	
		dto.setTargetFunctionCodes(functionCodesDto);
		dto.setTargetResponse(responseDto);
		dto.setTargetConfig(configDto);

		return dto;
	}
}	















