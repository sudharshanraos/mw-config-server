
package com.tarang.middleware.configserver.utils;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import com.tarang.middleware.configserver.dto.MessageConfigDto;
import com.tarang.middleware.configserver.dto.SourceConfigDetailsDto;
import com.tarang.middleware.configserver.dto.SourceConfigDto;
import com.tarang.middleware.configserver.dto.SourceDetails;
import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;
import com.tarang.middleware.configserver.dto.SourceMessageMappingDto;
import com.tarang.middleware.configserver.dto.SourceResponseDto;
import com.tarang.middleware.configserver.entity.MessageConfigEntity;
import com.tarang.middleware.configserver.entity.SourceConfigEntity;
import com.tarang.middleware.configserver.entity.SourceFunctionCodeEntity;
import com.tarang.middleware.configserver.entity.SourceMessageMappingEntity;
import com.tarang.middleware.configserver.entity.SourceResponseEntity;

public class SourceConfigDetailUtils {
	
	
	public static SourceDetails getSourceConfigModel(SourceConfigEntity srcEntity, MessageConfigEntity msgConfigEntity)
	{
		SourceDetails srcDetails = new SourceDetails();
		SourceConfigDto srcConfig = new SourceConfigDto();
		MessageConfigDto msgConfig = new MessageConfigDto();
		
		srcConfig.setAvailableTargets(srcEntity.getAvailableTargets());
		srcConfig.setConnectionTimeout(srcEntity.getConnectionTimeout());
		srcConfig.setConnectionType(srcEntity.getConnectionType());
		srcConfig.setDecryptKey(srcEntity.getDecryptKey());
		srcConfig.setDecryptType(srcEntity.getDecryptType());
		srcConfig.setDefaultTarget(srcEntity.getDefaultTarget());
		srcConfig.setEncryptKey(srcEntity.getEncryptKey());
		srcConfig.setEncryptType(srcEntity.getEncryptType());
		srcConfig.setHeaders(srcEntity.getHeaders());
		srcConfig.setId(srcEntity.getId());
		srcConfig.setIsoPackager(srcEntity.getIsoPackager());
		srcConfig.setJsonPackager(srcEntity.getJsonPackager());
		srcConfig.setMerchantId(srcEntity.getMerchantId());
		srcConfig.setMessageFormat(srcEntity.getMessageFormat());
		srcConfig.setPort(srcEntity.getPort());
		srcConfig.setRequestTimeout(srcEntity.getRequestTimeout());
		srcConfig.setSourceName(srcEntity.getSourceName());
		srcConfig.setTransactionLog(srcEntity.getTransactionLog());
		srcConfig.setUri(srcEntity.getUri());
		srcConfig.setValidation(srcEntity.getValidation());
		srcConfig.setValidationCheck(srcEntity.getValidationCheck());
		srcConfig.setXmlPackager(srcEntity.getXmlPackager());
		
		msgConfig.setId(msgConfigEntity.getId());
		msgConfig.setIsoRequest(msgConfigEntity.getIsoRequest());
		msgConfig.setJsonRequest(msgConfigEntity.getJsonRequest());
		msgConfig.setJsonResponse(msgConfigEntity.getJsonResponse());
		msgConfig.setTransactionType(msgConfigEntity.getTransactionType());
		msgConfig.setXmlRequest(msgConfigEntity.getXmlRequest());
		msgConfig.setXmlResponse(msgConfigEntity.getXmlResponse());
		srcDetails.setSourceObj(srcConfig);
		srcDetails.setMessageObj(msgConfig);
		return srcDetails;
	}
	
	
	public static SourceConfigDetailsDto getSourceConfigDto(
			SourceConfigEntity config,List<SourceFunctionCodeEntity> functionCodeDetails,
			List<SourceMessageMappingEntity> messageDetails, List<SourceResponseEntity> responseDetails)
	{
	    ModelMapper modelMapper = new ModelMapper();
		List<SourceFunctionCodeDto> functionCodesDto = new ArrayList<>();
		List<SourceMessageMappingDto> messageMappingDto = new ArrayList<>();
		List<SourceResponseDto> responseDto = new ArrayList<>();
        SourceConfigDetailsDto dto  = new SourceConfigDetailsDto();
		for(SourceFunctionCodeEntity function : functionCodeDetails) {
			SourceFunctionCodeDto funDto = modelMapper.map(function, SourceFunctionCodeDto.class);
          functionCodesDto.add(funDto);
          }
		 for(SourceMessageMappingEntity message : messageDetails) {
		        SourceMessageMappingDto messageDto = modelMapper.map(message,SourceMessageMappingDto.class);
				messageMappingDto.add(messageDto);
				}
		 for(SourceResponseEntity response:responseDetails) {
				SourceResponseDto sourceDto=modelMapper.map(response,SourceResponseDto.class);
				responseDto.add(sourceDto);
				
				}
	        SourceConfigDto configDto=modelMapper.map(config, SourceConfigDto.class);
	        

  
        
		dto.setSourceFunction(functionCodesDto);
		dto.setSourceMessage(messageMappingDto);
	    dto.setSourceResponse(responseDto);
	    dto.setSourceConfig(configDto);
		
		return dto;
		
	}
	

	}
	
	



