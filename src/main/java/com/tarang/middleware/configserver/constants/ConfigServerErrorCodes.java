package com.tarang.middleware.configserver.constants;

/**
 * The Enum ConfigServerErrorCodes.
 * 
 * @author sudharshan.s
 */
public enum ConfigServerErrorCodes {

    /** The success. */
    SUCCESS(200, "success");

    /** The code. */
    private final int code;

    /** The description. */
    private final String description;

    /**
     * Instantiates a new config server error codes.
     *
     * @param code
     *            the code
     * @param description
     *            the description
     */
    ConfigServerErrorCodes(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
