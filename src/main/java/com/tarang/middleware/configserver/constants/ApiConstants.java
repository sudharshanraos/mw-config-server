package com.tarang.middleware.configserver.constants;

/**
 * The Interface ApiConstants.
 * 
 * @author sudharshan.s
 */
public final class ApiConstants {

	/** The Constant PARENT. */
	public static final String PARENT = "/";

	/** The Constant MERCHANT_CONFIG_MANAGEMENT. */
	public static final String MERCHANT_CONFIG_MANAGEMENT = "Merchant Configuration Management";

	/** The Constant MERCHANT_PARENT. */
	public static final String MERCHANT_PARENT = "/merchant";

	/** The Constant MERCHANT_ADD_CONFIG. */
	public static final String MERCHANT_ADD_CONFIG = "/addconfig";

	/** The Constant MERCHANT_UPDATE_CONFIG. */
	public static final String MERCHANT_UPDATE_CONFIG = "/updateconfig";

	/** The Constant MERCHANT_LIST. */
	public static final String MERCHANT_LIST = "/list";

	/** The Constant MERCHANT_LIST_BY_ID. */
	public static final String MERCHANT_LIST_BY_ID = "/listbyid";

	/** The Constant MERCHANT_DELETE_CONFIG. */
	public static final String MERCHANT_DELETE_CONFIG = "/deleteconfig";

	/** The Constant MESSAGE_CONFIG_MANAGEMENT. */
	public static final String MESSAGE_CONFIG_MANAGEMENT = "Message Configuration Management";

	/** The Constant MESSAGE_PARENT. */
	public static final String MESSAGE_PARENT = "/message";

	/** The Constant MESSAGE_ADD_CONFIG. */
	public static final String MESSAGE_ADD_CONFIG = "/addmessageconfig";

	/** The Constant MESSAGE_UPDATE_CONFIG. */
	public static final String MESSAGE_UPDATE_CONFIG = "/updatemessageconfig";

	/** The Constant MESSAGE_LIST. */
	public static final String MESSAGE_LIST = "/list";

	/** The Constant MESSAGE_LIST_BY_ID. */
	public static final String MESSAGE_LIST_BY_ID = "/listbyid";

	/** The Constant MESSAGE_DELETE_CONFIG. */
	public static final String MESSAGE_DELETE_CONFIG = "/deletemessageconfig";

	/** The Constant SOURCE_CONFIG_MANAGEMENT. */
	public static final String SOURCE_CONFIG_MANAGEMENT = "Source Configuration Management";

	/** The Constant SOURCE_PARENT. */
	public static final String SOURCE_PARENT = "/source";

	/** The Constant SOURCE_ADD_CONFIG. */
	public static final String SOURCE_ADD_CONFIG = "/addsourceconfig";

	/** The Constant SOURCE_UPDATE_CONFIG. */
	public static final String SOURCE_UPDATE_CONFIG = "/updatesourceconfig";

	/** The Constant SOURCE_LIST. */
	public static final String SOURCE_LIST = "/list";

	/** The Constant SOURCE_LIST_BY_ID. */
	public static final String SOURCE_LIST_BY_ID = "/listbyid";

	/** The Constant SOURCE_DELETE_CONFIG. */
	public static final String SOURCE_DELETE_CONFIG = "/deletesourceconfig";

	/** The Constant SYSTEM_CONFIG_MANAGEMENT. */
	public static final String SYSTEM_CONFIG_MANAGEMENT = "System Configuration Management";

	/** The Constant SYSTEM_PARENT. */
	public static final String SYSTEM_PARENT = "/system";

	/** The Constant SYSTEM_GET_ENCRYPT_DATA. */
	public static final String SYSTEM_GET_ENCRYPT_DATA = "/encrypt";

	/** The Constant SYSTEM_GET_DECRYPT_DATA. */
	public static final String SYSTEM_GET_DECRYPT_DATA = "/decrypt";

	/** The Constant TARGET_CONFIG_MANAGEMENT. */
	public static final String TARGET_CONFIG_MANAGEMENT = "Target Configuration Management";

	/** The Constant TARGET_PARENT. */
	public static final String TARGET_PARENT = "/target";

	/** The Constant TARGET_ADD_CONFIG. */
	public static final String TARGET_ADD_CONFIG = "/addTargetConfig";

	/** The Constant TARGET_UPDATE_CONFIG. */
	public static final String TARGET_UPDATE_CONFIG = "/updateTargetConfig";

	/** The Constant TARGET_LIST. */
	public static final String TARGET_LIST = "/list";

	/** The Constant TARGET_LIST_BY_ID. */
	public static final String TARGET_LIST_BY_TARGET_NAME = "/listByTargetName";

	/** The Constant TARGET_DELETE_CONFIG. */
	public static final String TARGET_DELETE_CONFIG = "/deleteTargetConfig";

	public static final String GET_SOURCE_MSG_DETAILS = "/sourceMsgDetails";

	public static final String SOURCE_RESPONSE_PARENT ="/sourceResponse";

	public static final String SOURCE_RESPONSE_ADD_CONFIG = "/addSourceResponse";

	public static final String SOURCE_TABLE="/sourceResponse";

	public static final String UPDATE_SOURCE_RESPONSE ="/updatesourceresponse";

	public static final String SOURCE_RESPONSE_MANAGEMENT = "Source Responses Management";

	public static final String SOURCE_RESPONSE_LIST ="/list";

	public static final String SOURCE_RESPONSE_LIST_BY_ID ="/listbyid";

	public static final String SOURCE_RESPONSE_DELETE = "/deletesourceresponse";

	public static final String RESPONSE_DELETE = "/deleteresponse";

	public static final String TARGET_RESPONSE_PARENT="/targetresponse";

	public static final String TARGET_RESPONSE_ADD_CONFIG = "/addTargetResponse";

	public static final String TARGET_RESPONSE_MANAGEMENT = "/targetResponsesManagement";

	public static final String UPDATE_TARGET_RESPONSE="/updateTargetResponse";

	public static final String TARGET_RESPONSE_LIST="/list";

	public static final String TARGET_RESPONSE_LIST_BY_ID="/listById";

	public static final String TARGET_RESPONSE_DELETE="/deleteTargetResponse";

	public static final String SOURCE_FUNCTION_CODE_PARENT="/sourceFunctionCode";

	public static final String SOURCE_FUNCTION_CODE_MANAGEMENT="/sourceFunctionCodeManagement";

	public static final String SOURCE_FUNCTION_CODE_ADD_CONFIG="/addSourceFunctionCode";

	public static final String UPDATE_SOURCE_FUNCTION_CODE="/updateSourceFunctionCode";

	public static final String SOURCE_FUNCTION_CODE_LIST ="/list";

	public static final String SOURCE_FUNCTION_CODE_LIST_BY_ID="/listById";

	public static final String SOURCE_FUNCTION_CODE_DELETE = "/deleteSourceFunctionCode";
	
	public static final String TARGET_FUNCTION_CODE_ADD_PARENT="/targetFunctionCode";
	
	public static final String TARGET_FUNCTION_CODE_MANAGEMENT="/targetFunctionCodeManagement";
	
	public static final String TARGET_FUNCTION_CODE_ADD_CONFIG="/addTargetFunctionCode";
	
	public static final String UPDATE_TARGET_FUNCTION_CODE = "/updateTargetFunctionCode";
	
	public static final String TARGET_FUNCTION_CODES_LIST = "/list";
	
	public static final String TARGET_FUNCTION_CODES_LIST_BY_ID = "/listById";
	
	public static final String TARGET_FUNCTION_CODES_DELETE = "/delete";
	
	public static final String GET_SOURCE_CONFIG = "/getSourceConfigDetails";
	
	public static final String GET_TARGET_CONFIG = "/getTargetConfigDetails";
		
	public static final String SAVE_SOURCE_DETAILS ="/saveSourceDetails";
	
	public static final String SAVE_TARGET_DETAILS ="/saveTargetDetails";
	
    public static final String ERROR_INVALID_MERCHANT_ID = "error.invalid.merchantId";
	
	public static final String ERROR_INVALID_SOURCE_NAME = "error.invalid.sourceName";
	
	public static final String TARGEET_ID ="id";
	
	public static final String ERROR_INVALID_ID ="error.invalid.id";
	
	public static final String ERROR_INVALID_UUID="error.invalid.uuid";

	public static final String CURRENCY_LIST = "/list";
			
	public static final String CURRENCY_PARENT="/currency";
	
	public static final String CURRENCY_MANAGEMENT="currencyManagement";
			
	public static final String USERS_ADD_PARENT="/users";
			
	public static final String USERS_MANAGEMENT="/users management";
			
	public static final String USERS_ADD_CONFIG="/addusers";
			
	public static final String UPDATE_USERS="/updateusers";
			
	public static final String USERS_LIST="/list";

	public static final String USERS_LIST_BY_ID="/listbyid";
		    
	public static final String USERS_DELETE="/delete";
		    

	/**
	 * Instantiates a new Api constants.
	 */
	private ApiConstants() {
		// private constructor
	}

}
