package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.tarang.middleware.configserver.enums.Status;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class MerchantConfig.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Entity
@Table(name = "MERCHANT_CONFIG", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "MERCHANT_ID"
        })
})
public class MerchantConfig implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /** The name. */
    @Column(name = "merchant_name")
    private String merchantName;

    /** The merchant id. */
    @Column(name = "MERCHANT_ID", nullable = false)
    private String merchantId;

    /** The terminal id. */
    @Column(name = "TERMINAL_ID", nullable = false)
    private String terminalId;
    
    /** The status. */
    @Column(name = "STATUS")
    private String status;
    
    @Column(name = "additional_data")
    private String additionalData;
    
    

    /** The auditlog. */
    @Embedded
    private AuditLog auditlog;

}
