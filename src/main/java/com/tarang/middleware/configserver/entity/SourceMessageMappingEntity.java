
package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "SOURCE_MESSAGE_MAPPING", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "SOURCE_ID","MESSAGE_ID","TRANSACTION_TYPE"
        })
})
public class SourceMessageMappingEntity implements Serializable{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "SOURCE_ID", nullable = false)
    private String sourceId;
    
    @Column(name = "MESSAGE_ID", nullable = false)
    private Long messageId;
    
    @Column(name = "TRANSACTION_TYPE", nullable = false)
    private String transactionType;
    
    /** The auditlog. */
    @Embedded
    private AuditLog auditlog;
    

}
