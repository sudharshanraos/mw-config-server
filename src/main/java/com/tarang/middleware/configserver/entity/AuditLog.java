package com.tarang.middleware.configserver.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class AuditLog.
 * 
 * @author sudharshan.s
 */

@Embeddable
public class AuditLog implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The created by. */
    @Column(name = "CREATED_BY")
    private String createdBy;

    /** The created at. */
    @Column(name = "CREATED_AT")
    private Date createdAt;

    /** The updated by. */
    @Column(name = "UPDATED_BY")
    private String updatedBy;

    /** The updated at. */
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

}
