
package com.tarang.middleware.configserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;



/**
 * The Class TargetResponseEntity.
 * 
 * @author venkatramanap.
 *
 */
@Setter
@Getter
@Entity
@Table(name = "TARGET_RESPONSE", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "TARGET_ID"
        })
})
public class TargetResponseEntity implements Serializable {

    /** The Constant SerialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @Column(name = "id")
    private int id;

    /** The target id. */
    @Column(name = "target_id")
    private String targetId;

    /** The response code. */
    @Column(name = "response_code")
    private String responseCode;

    /** The response message. */
    @Column(name = "response_message")
    private String responseMessage;
    
    /** The language code. */
    @Column(name = "language_code")
    private String languageCode;
    
	/** The createdBy.*/
    @Column(name = "created_by")
	private String createdBy;
	
	/** The createdat.*/
    @Column(name = "created_at")
	private Date createdat;

	/** The updatedBy.*/
    @Column(name = "updated_by")
	private String updatedBy;
	
	/** The updatedAt.*/
    @Column(name = "updated_at")
	private Date updatedAt;

    
}
