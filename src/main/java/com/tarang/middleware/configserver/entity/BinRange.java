package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class BinRange.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Entity
@Table(name = "BIN_RANGE")
public class BinRange implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /** The bin. */
    @Column(name = "BIN")
    private String bin;

    /** The range. */
    @Column(name = "RANGE")
    private String range;

    /** The description. */
    @Column(name = "DESCRIPTION")
    private String description;

    /** The pan length. */
    @Column(name = "PAN_LENGTH")
    private String panLength;

}
