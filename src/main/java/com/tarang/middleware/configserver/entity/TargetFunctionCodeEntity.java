package com.tarang.middleware.configserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;



/**
 * The Class TargetFunctionCodesEntity.
 * 
 * @author venkatramanap.
 *
 */
@Setter
@Getter
@Entity
@Table(name="Target_Function_Code" , uniqueConstraints= {@UniqueConstraint(columnNames={"iD"})})

public class TargetFunctionCodeEntity implements Serializable {


	/** The Constant serialVersionUID.*/
	private static final long serialVersionUID=1L;



	/** The id.*/
	@Id
	@Column(name="id")
	private int id;


	/** The targetId.*/
	@Column(name="target_id")
	private String targetID;


	/** The languageCode.*/
	@Column(name="language_code")
	private String languageCode;


	/** The functionCode.*/
	@Column(name="function_code")
	private String functionCode;


	/** The functionMessage.*/
	@Column(name="function_message")
	private String functionMessage;


	/** The createdBy.*/
	@Column(name="created_by")
	private String createdBy;


	/** The createdat.*/
	@Column(name="created_at")
	private Date createdat;


	/** The updatedBy.*/
	@Column(name="updated_by")
	private String updatedBy;


	/** The updatedat.*/
	@Column(name="updated_at")
	private Date updatedat;


}

