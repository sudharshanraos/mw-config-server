package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class SourceConfig.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Entity
@Table(name = "SOURCE_CONFIG")
public class SourceConfigEntity implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    /** The source id.*/
    @Column(name="SOURCE_ID")
    private String 	sourceId;

    /** The source name. */
    @Column(name = "SOURCE_NAME", nullable = false)
    private String sourceName;
    
    /** The port. */
    @Column(name = "PORT")
    private String port;

    /** The merchant id. */
    @Column(name = "MERCHANT_ID")
    private String merchantId;
    
    /** The merchant name.*/
    @Column(name="MERCHANT_NAME")
    private String merchantName;
    
    /** The available targets. */
    @Column(name = "AVAILABLE_TARAGETS")
    private String availableTargets;

    /** The default target. */
    @Column(name = "DEFAULT_TARGET")
    private String defaultTarget;

    /** The connection type. */
    @Column(name = "CONNECTION_TYPE", nullable = false)
    private String connectionType;
    
    /** The uri. */
    @Column(name = "URI")
    private String uri;
    
    /** The connection timeout. */
    @Column(name = "CONNECTION_TIMEOUT")
    private String connectionTimeout;

    /** The Request timeout. */
    @Column(name = "REQUEST_TIMEOUT")
    private String requestTimeout;
    
    /** The encrypt type. */
    @Column(name = "ENCRYPT_TYPE")
    private String encryptType;
    
    /** The encrypt key. */
    @Column(name = "ENCRYPT_KEY")
    private String encryptKey;
    
    /** The decrypt type. */
    @Column(name = "DECRYPT_TYPE")
    private String decryptType;
    
    /** The decrypt key. */
    @Column(name = "DECRYPT_KEY")
    private String decryptKey;
    
    /** The message format. */
    @Column(name = "MESSAGE_FORMAT", nullable = false)
    private String messageFormat;

    /** The headers. */
    @Column(name = "HEADERS")
    private String headers;

    /** The iso packager. */
    @Column(name = "ISO_PACKAGER")
    private String isoPackager;

    /** The json packager. */
    @Column(name = "JSON_PACKAGER")
    private String jsonPackager;
    
    /** The transaction log. */
    @Column(name = "TRANSACTION_LOG")
    private String transactionLog;
 
    /** The validation. */
    @Column(name = "VALIDATION")
    private String validation;

    /** The validation check. */
    @Column(name = "VALIDATION_CHECK")
    private String validationCheck;

   
    /** The auditlog. */
    @Embedded
    private AuditLog auditlog;

    /** The xml packager. */
    @Column(name = "XML_PACKAGER")
    private String xmlPackager;

	
    
    
    

}
