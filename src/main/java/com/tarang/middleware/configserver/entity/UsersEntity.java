package com.tarang.middleware.configserver.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;
/**
 * The Class UserEntity.
 * 
 * @author venkatramanap.
 *
 */
@Setter
@Getter
@Entity
@Table(name="USERS" , uniqueConstraints= {@UniqueConstraint(columnNames={"UUID"})})

public class UsersEntity implements Serializable {


	/** The Constant serialVersionUID.*/
	private static final long serialVersionUID=1L;



	/** The id.*/
	@Id
	@Column(name="id")
	private int id;
	

	/** The uuid.*/
	@Column(name="uuid")
	private String uuid;

	
	/** The login.*/
	@Column(name="login")
	private String login;
	
	
	/** The userName.*/
	@Column(name="user_name")
	private String userName;
	
	
	/** The email.*/
	@Column(name="email")
	private String email;
	
	
	/** The cryptedPassword.*/
	@Column(name="crypted_password")
	private String cryptedPassword;
	
	
	/** The hashMethod.*/
	@Column(name="hash_method")
	private String hashMethod;
	
	
	/** The secretkey.*/
	@Column(name="secret_key")
	private String secretKey;
	
	
	/** The active.*/
	@Column(name="active")
	private String active;
	
	
	/** The externalLogin.*/
	@Column(name="external_login")
	private String externalLogin;
	
	
	/** The externalId.*/
	@Column(name="external_id")
	private String externalId;


	/** The createdAt.*/
	@Column(name="created_at")
	private Date createdAt;


	/** The updatedAt.*/
	@Column(name="updated_at")
	private Date updatedAt;


	/** The connectedAt.*/
	@Column(name="connected_at")
	private Timestamp connectedAt;
	
}


