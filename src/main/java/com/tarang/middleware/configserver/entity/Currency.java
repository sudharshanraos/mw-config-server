package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class Currency.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Entity
@Table(name = "CURRENCY")
public class Currency implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /** The name. */
    @Column(name = "NAME")
    private String name;

    /** The code. */
    @Column(name = "CODE")
    private String code;

    /** The description. */
    @Column(name = "DESCRIPTION")
    private String description;

    /** The exponent. */
    @Column(name = "EXPONENT")
    private String exponent;

}
