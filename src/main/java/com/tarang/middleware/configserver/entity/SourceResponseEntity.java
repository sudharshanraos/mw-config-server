package com.tarang.middleware.configserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class SourceResponseEntity.
 * 
 * @author venkatramanap
 *
 */
@Getter
@Setter
@Entity
@Table(name = "SOURCE_RESPONSE")
public class SourceResponseEntity implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    /** The source id. */
    @Column(name = "source_id")
    private String sourceId;

    /** The response code. */
    @Column(name = "response_code")
    private String responseCode;
    
    /** The response message. */
    @Column(name = "response_message")
    private String responseMessage;
    
    /** The language code. */
    @Column(name = "language_code")
    private String languageCode;

    /** The created by.*/
    @Column(name ="created_by")
	 private String createdBy;
	 
    /** The created at.*/
    @Column(name= "created_At")
	 private Date createdAt;
	 
    /** The updated by.*/
    @Column (name="updated_by")
	 private String updatedBy;
	 
    /** The updated at.*/	
    @Column (name="updated_at")
	 private Date updatedAt;

}
