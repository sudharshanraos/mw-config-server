package com.tarang.middleware.configserver.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
@Entity
@Table(name="SOURCE_FUNCTIONCODE",uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "SOURCE_ID"
        })
})
public class SourceFunctionCodeEntity implements Serializable {
	
	/** The constant serialVersionUID*/
	private static final long serialVersionUID=1L;
	
	/** The id.*/
	@Id
	@Column(name="id")
	private int id;
	
	
	/** The source id.*/
	@Column(name="source_id")
	private String sourceId;
	
	
	/** The message id.*/
	@Column(name="message_id")
	private String messageId;
	
	
    /** The function code.*/
    @Column(name="function_code")
    private String functionCode;


   /** The created by.*/
   @Column(name="created_by")
   private String createdBy;


   /** The created at.*/
   @Column(name="created_at")
   private Date createdAt;


   /** The updated by.*/
   @Column(name="updated_by")
   private String updatedBy;


   /** The updated at.*/
   @Column(name="updated_at")
   private Date updatedAt;



}
