package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class MessageConfig.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Entity
@Table(name = "MESSAGE_CONFIG" ,uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "ID"
        })
})
public class MessageConfigEntity implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    
    
    /** The iso request. */
    @Column(name = "ISO_REQUEST")
    private String isoRequest;

    /** The iso response. */
    @Column(name = "ISO_RESPONSE")
    private String isoResponse;
    
    
    /** The json request. */
    @Column(name = "JSON_REQUEST")
    private String jsonRequest;

    /** The json response. */
    @Column(name = "JSON_RESPONSE")
    private String jsonResponse;
    
    
    /** The transaction type. */
    @Column(name = "TRANSACTION_TYPE", nullable = false, length = 20)
    private String transactionType;

    

    /** The xml request. */
    @Column(name = "XML_REQUEST")
    private String xmlRequest;

    /** The xml response. */
    @Column(name = "XML_RESPONSE")
    private String xmlResponse;

  

    /** The auditlog. */
    @Embedded
    private AuditLog auditlog;

}
