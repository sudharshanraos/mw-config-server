package com.tarang.middleware.configserver.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Getter;
import lombok.Setter;

/**
 * The Class TargetConfig.
 * 
 * @author sudharshan.s
 */
@Getter
@Setter
@Entity
@Table(name = "TARGET_CONFIG", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
               "TARGET_ID", "TARGET_NAME"
        })
})
public class TargetConfigEntity implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    /** The target id.*/
    @Column(name="TARGET_ID")
    private String targetId;

    /** The target name. */
    @Column(name = "TARGET_NAME", nullable = false)
    private String targetName;

    /** The merchant id. */
    @Column(name = "MERCHANT_ID")
    private String merchantId;
    
    /** The merchant name.*/
    @Column(name="MERCHANT_NAME")
    private String merchantName;

    /** The host. */
    @Column(name = "HOST")
    private String host;
    
    /** The port. */
    @Column(name = "PORT")
    private String port;
    
    /** The connection type. */
    @Column(name = "CONNECTION_TYPE", nullable = false)
    private String connectionType;
    
    /** The uri. */
    @Column(name = "URI")
    private String uri;
    
    /** The connection timeout. */
    @Column(name = "CONNECTION_TIMEOUT")
    private String connectionTimeout;

    /** The Request timeout. */
    @Column(name = "REQUEST_TIMEOUT")
    private String requestTimeout;
    
    /** The encrypt type. */
    @Column(name = "ENCRYPT_TYPE")
    private String encryptType;
    
    /** The encrypt key. */
    @Column(name = "ENCRYPT_KEY")
    private String encryptKey;
    
    /** The decrypt type. */
    @Column(name = "DECRYPT_TYPE")
    private String decryptType;
    
    /** The decrypt key. */
    @Column(name = "DECRYPT_KEY")
    private String decryptKey;
    
    /** The message format. */
    @Column(name = "MESSAGE_FORMAT", nullable = false)
    private String messageFormat;

    /** The headers. */
    @Column(name = "HEADERS")
    private String headers;

    /** The iso packager. */
    @Column(name = "ISO_PACKAGER")
    private String isoPackager;

    /** The json packager. */
    @Column(name = "JSON_PACKAGER")
    private String jsonPackager;

    /** The heartbeat. */
    @Column(name = "HEARTBEAT")
    private String heartbeat;

    /** The heartbeat dealy. */
    @Column(name = "HEARTBEAT_DELAY")
    private String heartbeatDealy;

    /** The retry. */
    @Column(name = "RETRY")
    private String retry;

    /** The retry count. */
    @Column(name = "RETRY_COUNT")
    private String retryCount;
    
    /** The schema type. */
    @Column(name = "SCHEMA_TYPE")
    private String schemaType;
    
    /** The sign in. */
    @Column(name = "SIGNIN")
    private String signIn;


    /** The auditlog. */
    @Embedded
    private AuditLog auditlog;
    
  
    /** The xml packager. */
    @Column(name = "XML_PACKAGER")
    private String xmlPackager;

}
