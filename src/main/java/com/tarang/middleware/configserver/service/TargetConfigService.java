package com.tarang.middleware.configserver.service;

import java.util.List;

import com.tarang.middleware.configserver.dto.TargetConfigDetailsDto;
import com.tarang.middleware.configserver.dto.TargetConfigDto;

/**
 * The Interface TargetConfigService.
 * 
 * @author sudharshan.s
 */
public interface TargetConfigService {

	/**
	 * Adds the target config.
	 *
	 * @param model
	 *            the model
	 * @return the target config dto
	 */
	TargetConfigDto addTargetConfig(TargetConfigDto model);

	/**
	 * Update target config.
	 *
	 * @param mmm
	 *            the mmm
	 * @return the target config dto
	 */
	TargetConfigDto updateTargetConfig(TargetConfigDto mmm);

	/**
	 * Delete target config.
	 *
	 * @param model
	 *            the model
	 */
	void deleteTargetConfig(TargetConfigDto model);

	/**
	 * Gets the target config list.
	 *
	 * @return the target config list
	 */
	List<TargetConfigDto> getTargetConfigList();

	/**
	 * Gets the target config by name.
	 *
	 * @param targetName
	 *            the target name
	 * @return the target config by name
	 */
	TargetConfigDto getTargetConfigByName(String targetName);

	/** 
	 * Get the port
	 * 
	 * @param port
	 *       The port
	 *       
	 * @return target config dto
	 */

	TargetConfigDetailsDto getAllTargetConfig(String port);
	/** 
	 * 
	 * @param model
	 *       the model
	 *       
	 * @return TargetConfigDetailsDto
	 */
	
TargetConfigDetailsDto saveTargetConfigDetails(TargetConfigDetailsDto model);

}
