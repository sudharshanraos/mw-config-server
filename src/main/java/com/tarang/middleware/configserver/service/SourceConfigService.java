package com.tarang.middleware.configserver.service;

import java.util.List;

import com.tarang.middleware.configserver.dto.SourceConfigDetailsDto;
import com.tarang.middleware.configserver.dto.SourceConfigDto;
import com.tarang.middleware.configserver.dto.SourceDetails;

/**
 * The Interface SourceConfigService.
 * 
 * @author sudharshan.s
 */
public interface SourceConfigService {

    /**
     * Adds the source config.
     *
     * @param model
     *            the model
     * @return the source config dto
     */
    SourceConfigDto addSourceConfig(SourceConfigDto model);

    /**
     * Update source config.
     *
     * @param mmm
     *            the mmm
     * @return the source config dto
     */
    SourceConfigDto updateSourceConfig(SourceConfigDto mmm);

    /**
     * Delete source config.
     *
     * @param model
     *            the model
     */
    void deleteSourceConfig(SourceConfigDto model);

    /**
     * Gets the source config list.
     *
     * @return the source config list
     */
    List<SourceConfigDto> getSourceConfigList();

    /**
     * Gets the source config by name.
     *
     * @param targetName
     *            the target name
     * @return the source config by name
     */
    SourceConfigDto getSourceConfigByName(String sourceName);

    /**
     * Gets the source details.
     *
     * @param port
     *            the port
     * @return the source details
     */

    SourceDetails getSourceDetails(String port);

    
    
    /**
     * Gets the source details
     * 
     * @param port
     *       The port
     *       
     * @return source config details dto
     */
    
    SourceConfigDetailsDto getAllSourceConfig(String port);

    
    
    SourceConfigDetailsDto saveSourceConfigDetails(SourceConfigDetailsDto sourceDto);

	


}
