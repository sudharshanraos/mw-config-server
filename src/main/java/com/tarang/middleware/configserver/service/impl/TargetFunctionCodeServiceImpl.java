package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarang.middleware.configserver.dto.TargetFunctionCodeDto;
import com.tarang.middleware.configserver.entity.TargetFunctionCodeEntity;
import com.tarang.middleware.configserver.repository.TargetFunctionCodeRepository;
import com.tarang.middleware.configserver.service.TargetFunctionCodeService;

/**
 * @author venkatramanap
 *
 */
@Component
public class TargetFunctionCodeServiceImpl implements TargetFunctionCodeService{
	
	  
	/** The constant logger.*/
	private static final Logger LOGGER = LoggerFactory.getLogger(TargetFunctionCodeServiceImpl.class);
	
	

	/** The repository.*/
	@Autowired
	private TargetFunctionCodeRepository repository;
	
	
	/** The model mapper.*/
	@Autowired
	ModelMapper modelMapper;
	
	
	/** The message source.*/
	@Autowired
	MessageSource messageSource;


	
    /** 
     * Add the target function model.
     * 
     * @param the target function model.
     * 
     * @return the target function codes dto.
     * 
     */
	public TargetFunctionCodeDto addTargetFunctionCodes(TargetFunctionCodeDto targetFunctionModel) {
		LOGGER.info("------TargetFunctionCodesServiceImpl / addTargetFunctionCodesService()-----------Started");
		targetFunctionModel.setCreatedAt(new Date());
		TargetFunctionCodeEntity entity = modelMapper.map(targetFunctionModel,TargetFunctionCodeEntity.class );
		TargetFunctionCodeEntity codesEntity = repository.save(entity);
		TargetFunctionCodeDto dto = modelMapper.map(codesEntity,TargetFunctionCodeDto.class );
	     LOGGER.info("------TargetFunctionCodesServiceImpl / addTargetFunctionCodes()-------end");
		return dto;
	}

	/**
	 * Get the  id.
	 * 
	 * @param the  id.
	 * 
	 * @return the target function codes dto.
	 *
	 */
	@Override
	public TargetFunctionCodeDto getById(int id) {
		LOGGER.info("-------TargetFunctionCodesServiceImpl / getByid()--------------Started");
		TargetFunctionCodeEntity entity=repository.findById(id);
		if(entity==null) {
		       throw new EntityNotFoundException(messageSource.getMessage("error.invalid.id", new Object[] {
							Integer.valueOf(id)
					}, Locale.getDefault()));		
		}
TargetFunctionCodeDto dto = modelMapper.map(entity, TargetFunctionCodeDto.class);
LOGGER.info("-------TargetFunctionCodesServiceImpl / getByid()-------------end");
return dto;
	}
	
	
	/**
	 * update target function codes.
	 * 
	 * @param the target function Model.
	 * 
	 * @return the target function codes dto.
	 */
	@Override
  public TargetFunctionCodeDto updateFunctionCodes(TargetFunctionCodeDto targetFunctionModel) {
		targetFunctionModel.setUpdatedAt(new Date());
		LOGGER.info("------TargetFunctionCodesServiceImpl / updateFunctionCodes()---------Starting");
		TargetFunctionCodeEntity entity =modelMapper.map(targetFunctionModel,TargetFunctionCodeEntity.class );
		TargetFunctionCodeEntity codesdto=repository.save(entity);
		TargetFunctionCodeDto dto=modelMapper.map(codesdto,TargetFunctionCodeDto.class);
		LOGGER.info("--------TargetFunctionCodesServiceImpl / updateFunctionCodes()-------end");
		return dto;
	}
	

	/**
	 * Get the target function codes list.
	 * 
	 *@return the target function codes list.
	 */
	@Override
	public HashMap<String, List<TargetFunctionCodeDto>> getTargetFunctionCodesList() {
		HashMap<String,List<TargetFunctionCodeDto>> map = new HashMap<>();
		LOGGER.info("-------TargetFunctionCodesServiceImpl / getTargetFunctionCodes()-----String");
		List<String>languages=repository.getLanguages();
		List<TargetFunctionCodeEntity>entity=repository.findAll();
		List<TargetFunctionCodeDto> codesDto=new ArrayList<>();
		for(String language:languages) {
			codesDto.clear();
			for(TargetFunctionCodeEntity codesentity:entity) {
				if(language.equals(codesentity.getLanguageCode())) {
				TargetFunctionCodeDto dto =modelMapper.map(codesentity,TargetFunctionCodeDto.class);
				codesDto.add(dto);
			}
		}
		map.put(language, codesDto);
	}
		LOGGER.info("-----TargetFunctionCodesServiceImpl / getTargetFunctionCodes()------end");
	return map;
	}
	
	

	/**
	 *Get the getByTargetIdAndLanguageCode.
	 *
	 *@param int Id and languageCode
	 *      The Id and languageCode.
	 *      
	 */
	@Override
	public List<TargetFunctionCodeDto> getByTargetIdAndLanguageCode(int id, String languageCode) {
		LOGGER.info("--------TargetFunctionCodesServiceImpl / getBIdAndLanguageCode()---------Started");
		List<TargetFunctionCodeEntity> entity =repository.findByIdAndLanguageCode(id, languageCode);
		List<TargetFunctionCodeDto> codesDto=new ArrayList<>();
		if(entity ==null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.id",new Object[] {
					id
			},Locale.getDefault()));	
		}
		
		for(TargetFunctionCodeEntity codesEntity:entity) {
			TargetFunctionCodeDto dto = modelMapper.map(codesEntity, TargetFunctionCodeDto.class);
			codesDto.add(dto);
		}
		LOGGER.info("-------TargetFunctionCodesServiceImpl / getByIdAndLanguageCode()-----------end");
		return codesDto;
	}

	/**
	 * delete the target function codes dto.
	 * 
	 *@param targetFunctionModel
	 *       the targetFunctionModel.
	 */
	@Override
	public void deleteTargetFunctionCodesDto(TargetFunctionCodeDto targetFunctionModel) {
		LOGGER.info("-------TargetFunctionCodesDto / deleteTargetFunctionCodesDto()-------------Started");
		TargetFunctionCodeEntity entity = modelMapper.map(targetFunctionModel,TargetFunctionCodeEntity.class);
		LOGGER.info("-------TargetFunctionCodesDto / deleteTargetFunctionCodesDto()--------------end");
		repository.delete(entity);
		
	}

	

}



