
package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.tarang.middleware.configserver.dto.SourceConfigDetailsDto;
import com.tarang.middleware.configserver.dto.SourceConfigDto;
import com.tarang.middleware.configserver.dto.SourceDetails;
import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;
import com.tarang.middleware.configserver.dto.SourceMessageMappingDto;
import com.tarang.middleware.configserver.dto.SourceResponseDto;
import com.tarang.middleware.configserver.entity.MessageConfigEntity;
import com.tarang.middleware.configserver.entity.SourceConfigEntity;
import com.tarang.middleware.configserver.entity.SourceFunctionCodeEntity;
import com.tarang.middleware.configserver.entity.SourceMessageMappingEntity;
import com.tarang.middleware.configserver.entity.SourceResponseEntity;
import com.tarang.middleware.configserver.repository.MessageConfigRepository;
import com.tarang.middleware.configserver.repository.SourceConfigRepository;
import com.tarang.middleware.configserver.repository.SourceFunctionCodeRepository;
import com.tarang.middleware.configserver.repository.SourceMessageMappingRepository;
import com.tarang.middleware.configserver.repository.SourceResponseRepository;
import com.tarang.middleware.configserver.service.SourceConfigService;
import com.tarang.middleware.configserver.utils.SourceConfigDetailUtils;

/**
 * The Class SourceConfigServiceImpl.
 * 
 * @author sudharshan.s
 */
@Service
public class SourceConfigServiceImpl implements SourceConfigService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConfigServiceImpl.class);

	/** The repository. */
	@Autowired
	private SourceConfigRepository repository;

	/** The source function repo.*/ 
	@Autowired
	private SourceFunctionCodeRepository sourceFunctionRepo;

	/** The response repository.*/
	@Autowired
	private SourceResponseRepository responseRepository;

	/** The model mapper. */
	@Autowired
	ModelMapper modelMapper;

	/** The message source. */
	@Autowired
	MessageSource messageSource;

	/** The repository. */
	@Autowired
	private SourceMessageMappingRepository sourceMappingRepo;

	@Autowired
	private MessageConfigRepository msgConfigRepo;

	/**
	 * Adds the source config.
	 *
	 * @param model
	 *            the model
	 * @return the source config dto
	 */
	@Override
	public SourceConfigDto addSourceConfig(SourceConfigDto model) {
		LOGGER.debug("SourceConfigServiceImpl >>> addSourceConfig() ====start");
		SourceConfigEntity entity = modelMapper.map(model, SourceConfigEntity.class);
		SourceConfigEntity sourceEntity = repository.save(entity);
		SourceConfigDto sourceModel = modelMapper.map(sourceEntity, SourceConfigDto.class);
		LOGGER.debug("SourceConfigServiceImpl >>> addSourceConfig() ====ended");
		return sourceModel;
	}

	/**
	 * Update source config.
	 *
	 * @param model
	 *            the model
	 * @return the source config dto
	 */
	@Override
	public SourceConfigDto updateSourceConfig(SourceConfigDto model) {
		LOGGER.debug("SourceConfigServiceImpl >>> updateSourceConfig() ====started");
		SourceConfigEntity entity = modelMapper.map(model, SourceConfigEntity.class);
		SourceConfigEntity sourceEntity = repository.save(entity);
		SourceConfigDto sourceModel = modelMapper.map(sourceEntity, SourceConfigDto.class);
		LOGGER.debug("SourceConfigServiceImpl >>> updateSourceConfig() ====ended");
		return sourceModel;
	}

	/**
	 * Delete source config.
	 *
	 * @param model
	 *            the model
	 */
	@Override
	public void deleteSourceConfig(SourceConfigDto model) {
		LOGGER.debug("SourceConfigServiceImpl >>> deleteSourceConfig() ====started");
		SourceConfigEntity entity = modelMapper.map(model, SourceConfigEntity.class);
		repository.delete(entity);
		LOGGER.debug("SourceConfigServiceImpl >>> deleteSourceConfig() ====ended");

	}

	@Override
	public List<SourceConfigDto> getSourceConfigList() {
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceConfigList() ====started");
		List<SourceConfigEntity> entities = repository.findAll();
		List<SourceConfigDto> responseModel = new ArrayList<>();
		for (SourceConfigEntity SourceEntity : entities) {
			SourceConfigDto model = modelMapper.map(SourceEntity, SourceConfigDto.class);
			responseModel.add(model);
		}
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceConfigList() ====ended");
		return responseModel;
	}

	/**
	 * Gets the source config by name.
	 *
	 * @param SourceName
	 *            the source name
	 * @return the source config by name
	 */
	@Override
	public SourceConfigDto getSourceConfigByName(String sourceName) {
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceConfigById() ====started");
		SourceConfigEntity entity = repository.findBySourceName(sourceName);
		if (entity == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.name", new Object[] {
					sourceName
			}, Locale.getDefault()));
		}
		SourceConfigDto model = modelMapper.map(entity, SourceConfigDto.class);
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceConfigById() ====ended");
		return model;
	}

	@Override
	public SourceDetails getSourceDetails(String port) {
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceDetails() ====started");
		SourceConfigEntity entity = repository.getSourceConfig(port);
		SourceDetails sourceMappingDetails;
		if (entity == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.name", new Object[] {
					port
			}, Locale.getDefault()));
		}
		//Based on the SourceName ..need to get the Message Object
		Long messageID = sourceMappingRepo.getMessageID(entity.getSourceId());
		//Get the MessageConfig based on the MessageId
		MessageConfigEntity msgConfig = msgConfigRepo.getMessageConfig(messageID);
		sourceMappingDetails  = SourceConfigDetailUtils.getSourceConfigModel(entity,msgConfig);
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceConfigById() ====ended");
		return sourceMappingDetails;
	}

	@Override
	public SourceConfigDetailsDto getAllSourceConfig(String port) {
		LOGGER.info("-------SourceConfigServiceIMPl / getAllTables()------Started");
		SourceConfigEntity entity=repository.getSourceConfig(port);
		if(entity==null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.port", new Object[] {
					(port)

			},Locale.getDefault()));
		}
		String sourceId = entity.getSourceId();
		List<SourceFunctionCodeEntity> codeEntity =sourceFunctionRepo.getSourceFunctionCodeDetails(sourceId);
		List<SourceMessageMappingEntity> detail=sourceMappingRepo.getSourceMessageMapping(sourceId);
		List<SourceResponseEntity> responseEntity=responseRepository.getSourceResponseDetails(sourceId);
		SourceConfigDetailsDto  dto =SourceConfigDetailUtils.getSourceConfigDto(entity,codeEntity,detail,responseEntity);
		LOGGER.info("----SourceConfigServiceImpl / getAllSourceConfig()------end");
		return dto;
	}

	@Override
	public SourceConfigDetailsDto saveSourceConfigDetails(SourceConfigDetailsDto sourceDto) {
		LOGGER.info("------SourceConfigServiceImpl / getSourceConfigDetails()-------Started");
		List<SourceFunctionCodeDto> functionCodeDto = new ArrayList<>();
		List<SourceMessageMappingDto> sourceMappingDto = new ArrayList<>();
		List<SourceResponseDto> sourceResponseDto = new ArrayList<>();
		SourceConfigDetailsDto detailsDto = new SourceConfigDetailsDto();
		SourceConfigEntity entity=modelMapper.map(sourceDto.getSourceConfig(), SourceConfigEntity.class);
        SourceConfigEntity responseEntity=repository.save(entity);
		SourceConfigDto sourceConfigDto =modelMapper.map(responseEntity,SourceConfigDto.class);
		List<SourceFunctionCodeDto> source=sourceDto.getSourceFunction();
		for(SourceFunctionCodeDto codesEntity:source) {
			SourceFunctionCodeEntity functionCodes=modelMapper.map(codesEntity, SourceFunctionCodeEntity.class);
			functionCodes.setCreatedAt(new Date());
			SourceFunctionCodeEntity codes=sourceFunctionRepo.save(functionCodes);
			SourceFunctionCodeDto dto=modelMapper.map(codes, SourceFunctionCodeDto.class);
			functionCodeDto.add(dto);
		}

		List<SourceMessageMappingDto> mappingDto=sourceDto.getSourceMessage();
		for(SourceMessageMappingDto messageMapping:mappingDto) {
			SourceMessageMappingEntity detailEntity=modelMapper.map(messageMapping,SourceMessageMappingEntity.class);
			SourceMessageMappingEntity mapping=sourceMappingRepo.save(detailEntity);
			SourceMessageMappingDto dto=modelMapper.map(mapping,SourceMessageMappingDto.class);
			sourceMappingDto.add(dto);
		}

		List<SourceResponseDto> responseDto=sourceDto.getSourceResponse();
		for(SourceResponseDto sourceResponse : responseDto) {
			SourceResponseEntity sourceResponseEntity = modelMapper.map(sourceResponse, SourceResponseEntity.class);
			sourceResponseEntity.setCreatedAt(new Date());
			SourceResponseEntity sourceEntity=responseRepository.save(sourceResponseEntity);
			SourceResponseDto response=modelMapper.map(sourceEntity, SourceResponseDto.class);
			sourceResponseDto.add(response);
		}
        detailsDto.setSourceConfig(sourceConfigDto);      
		detailsDto.setSourceFunction(functionCodeDto);
		detailsDto.setSourceMessage(sourceMappingDto);
		detailsDto.setSourceResponse(sourceResponseDto);
		LOGGER.info("------SourceConfigServiceImpl / getSourceConfigDetails()-------end");
		return detailsDto;
	}
}
	
	
	


