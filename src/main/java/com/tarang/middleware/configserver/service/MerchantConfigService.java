package com.tarang.middleware.configserver.service;

import java.util.List;

import com.tarang.middleware.configserver.dto.MerchantConfigDto;

/**
 * The Interface MerchantConfigService.
 * 
 * @author sudharshan.s
 */
public interface MerchantConfigService {

    /**
     * Adds the merchant config.
     *
     * @param model
     *            the model
     * @return the merchant config dto
     */
    MerchantConfigDto addMerchantConfig(MerchantConfigDto model);

    /**
     * Update merchant config.
     *
     * @param mmm
     *            the mmm
     * @return the merchant config dto
     */
    MerchantConfigDto updateMerchantConfig(MerchantConfigDto mmm);

    /**
     * Delete merchant config.
     *
     * @param model
     *            the model
     */
    void deleteMerchantConfig(MerchantConfigDto model);

    /**
     * Gets the merchant config list.
     *
     * @return the merchant config list
     */
    List<MerchantConfigDto> getMerchantConfigList();

    /**
     * Gets the merchant config by id.
     *
     * @param merchantId
     *            the merchant id
     * @return the merchant config by id
     */
    MerchantConfigDto getMerchantConfigById(String merchantId);

}
