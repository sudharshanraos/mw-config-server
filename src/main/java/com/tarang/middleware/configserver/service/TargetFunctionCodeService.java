package com.tarang.middleware.configserver.service;

import java.util.HashMap;
import java.util.List;

import com.tarang.middleware.configserver.dto.TargetFunctionCodeDto;

/** The interface TargetFunctionCodesService.*/
public interface TargetFunctionCodeService {
	
	/**
	 * Adds the target function codes.
	 *
	 * @param target function model
	 *            the source function model.
	 *            
	 * @return the target function codes dto.
	 */
	TargetFunctionCodeDto addTargetFunctionCodes(TargetFunctionCodeDto targetFunctionModel);
	
	/**
	 * Get the id.
	 * 
	 * @param id
	 *        the id.
	 *        
	 * @return  by id.
	 */
	
	TargetFunctionCodeDto getById(int id);
	
	/**
	 * update target Function Model.
	 * 
	 * @param targetFunctionModel
	 *        The target Function model.
	 *        
	 * @return TargetFunctionCodesDto.
	 */
	
	TargetFunctionCodeDto updateFunctionCodes(TargetFunctionCodeDto targetFunctionModel);
	
	/** 
	 * 
	 * Gets the TargetFunctionCodesDto.
	 * 
	 * @return the TargetFunctionCodesDto.
	 */
	
	HashMap<String,List<TargetFunctionCodeDto>>getTargetFunctionCodesList();
	
	/**
	 * Get the id and languageCoide.
	 * 
	 * @param id and languageCode.
	 * 
	 * @return TargetFunctionCodesDto.
	 */
	
	List<TargetFunctionCodeDto> getByTargetIdAndLanguageCode(int id,String languageCode);
	
	/** 
	 * Delete the TargetFunctionCodesDto
	 * 
	 * @param targetFunctionModel
	 *         the targetFunctionModel
	 *         
	 */
	
	void deleteTargetFunctionCodesDto (TargetFunctionCodeDto targetFunctionModel);

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
