package com.tarang.middleware.configserver.service;

/**
 * The Interface ConfigServerMessageService.
 * 
 * @author sudharshan.s
 */
public interface ConfigServerMessageService {

    /**
     * Gets the message.
     *
     * @param errorCode
     *            the error code
     * @param objects
     *            the objects
     * @return the message
     */
    public String getMessage(final String errorCode, Object[] objects);

}
