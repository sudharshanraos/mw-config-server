package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;
import com.tarang.middleware.configserver.entity.SourceFunctionCodeEntity;
import com.tarang.middleware.configserver.repository.SourceFunctionCodeRepository;
import com.tarang.middleware.configserver.service.SourceFunctionCodeService;
/**
 * @author venkatramanap
 *
 */
@Component
public class SourceFunctionCodeServiceImpl implements SourceFunctionCodeService{

	/** the repository.*/
	@Autowired
	private SourceFunctionCodeRepository repository;

	/** The  Constant logger*/
	private static final Logger LOGGER= LoggerFactory.getLogger(SourceFunctionCodeServiceImpl.class);

	/** The modelMapper*/
	@Autowired
	ModelMapper modelMapper;


	/** The messageSource*/
	@Autowired
	MessageSource messageSource;



	/**
	 * Adds the source function codes.
	 *
	 * @param source function model
	 *            the source function model
	 * @return the source function codes dto
	 */
	@Override
	public SourceFunctionCodeDto addSourceFunctionCodes(SourceFunctionCodeDto sourceFunctionModel){
		LOGGER.info("-------SourceFunctionCodesserviceImpl/addSourceFunctionCodes()---------Started");
		sourceFunctionModel.setCreatedAt(new Date());
		SourceFunctionCodeEntity entity=modelMapper.map(sourceFunctionModel,SourceFunctionCodeEntity.class);
		SourceFunctionCodeEntity codesEntity=repository.save(entity);
		SourceFunctionCodeDto dto=modelMapper.map(codesEntity, SourceFunctionCodeDto.class);
		LOGGER.info("--------SourceFunctionCodesServiceImpl/addSourceFunctionCodes()--------end");
		return dto;

	}


	/**
	 * Gets the source by id.
	 *
	 * @param sourcetId
	 *            the source id
	 * @return the source by id
	 */
	@Override
	public SourceFunctionCodeDto getBysourceId(String sourceId) {
		LOGGER.info("--------SourceFunctionCodesServiceImpl/addSourceFunctionCodes()--------started");
		SourceFunctionCodeEntity codesEntity = repository.findBySourceId(sourceId);
		if (codesEntity == null) {       
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.sourceId", new Object[] {
					Integer.valueOf(sourceId)
			}, Locale.getDefault()));

		}
		SourceFunctionCodeDto dto = modelMapper.map(codesEntity, SourceFunctionCodeDto.class);
		LOGGER.info("--------SourceFunctionCodesServiceImpl/getBysourceId()--------end");
		return dto;
	}

	/**
	 * Update source function codes.
	 *
	 * @param source function code model
	 *            the source function code model
	 * @return the source function codes dto
	 */

	@Override
	public SourceFunctionCodeDto updateSourceFunctionCodes(SourceFunctionCodeDto sourceFunctionCodeModel) {
		sourceFunctionCodeModel.setCreatedAt(new Date());
		LOGGER.info("-------SourceFunctionCodesserviceImpl/updateSourceFunctionCodes()---------Started");

		SourceFunctionCodeEntity entity=modelMapper.map(sourceFunctionCodeModel, SourceFunctionCodeEntity.class);
		SourceFunctionCodeEntity codesEntity=repository.save(entity);
		SourceFunctionCodeDto codesDto=modelMapper.map(codesEntity,SourceFunctionCodeDto.class);
		LOGGER.info("-------SourceFunctionCodesserviceImpl / updateSourceFunctionCodes---end");

		return codesDto;
	}	

	/**
	 * Gets the source function codes list.
	 *
	 * @return the source function codes list
	 */
	@Override
	public List<SourceFunctionCodeDto> getSourceFunctionCodesList() {
		HashMap<String,List<SourceFunctionCodeDto>> map=new HashMap<>();
		LOGGER.info("-------SourceFunctionCodesserviceImpl/getSourceFunctionCodesList()---------Started");
		List<SourceFunctionCodeEntity> codesentity=repository.findAll();	
        	List<SourceFunctionCodeDto>codesDto= new ArrayList<>();
			for(SourceFunctionCodeEntity codesEntity :codesentity ) {
					SourceFunctionCodeDto dto=modelMapper.map(codesEntity, SourceFunctionCodeDto.class);
					codesDto.add(dto);
				
			}		
		LOGGER.info("-------SourceFunctionCodesserviceImpl/getSourceFunctionCodesList()---------end");

		return codesDto;
	}





	/**
	 * Delete source function codes
	 *
	 * @param source function model
	 *            the source function model
	 */

	@Override
	public void deleteSourceFunctionCodes(SourceFunctionCodeDto sourceFunctionModel) {
		LOGGER.info("-----SourceFunctionCodesServiceImpl / deleteSourceFunctionCodes()------Started");
		SourceFunctionCodeEntity entity =modelMapper.map(sourceFunctionModel, SourceFunctionCodeEntity.class);
		LOGGER.info("-----SourceFunctionCodesServiceImpl / deleteSourceFunctionCodes()------end");

		repository.delete(entity);

	}







}









