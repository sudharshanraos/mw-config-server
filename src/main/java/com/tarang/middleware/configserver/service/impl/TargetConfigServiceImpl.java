package com.tarang.middleware.configserver.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.tarang.middleware.configserver.dto.SourceConfigDetailsDto;
import com.tarang.middleware.configserver.dto.TargetConfigDetailsDto;
import com.tarang.middleware.configserver.dto.TargetConfigDto;
import com.tarang.middleware.configserver.dto.TargetFunctionCodeDto;
import com.tarang.middleware.configserver.dto.TargetResponseDto;
import com.tarang.middleware.configserver.entity.SourceConfigEntity;
import com.tarang.middleware.configserver.entity.SourceFunctionCodeEntity;
import com.tarang.middleware.configserver.entity.SourceMessageMappingEntity;
import com.tarang.middleware.configserver.entity.SourceResponseEntity;
import com.tarang.middleware.configserver.entity.TargetConfigEntity;
import com.tarang.middleware.configserver.entity.TargetFunctionCodeEntity;
import com.tarang.middleware.configserver.entity.TargetResponseEntity;
import com.tarang.middleware.configserver.repository.TargetConfigRepository;
import com.tarang.middleware.configserver.repository.TargetFunctionCodeRepository;
import com.tarang.middleware.configserver.repository.TargetResponseRepository;
import com.tarang.middleware.configserver.service.TargetConfigService;
import com.tarang.middleware.configserver.utils.SourceConfigDetailUtils;
import com.tarang.middleware.configserver.utils.TargetConfigDetailUtils;

/**
 * The Class TargetConfigServiceImpl.
 * 
 * @author sudharshan.s
 */
@Service
public class TargetConfigServiceImpl implements TargetConfigService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TargetConfigServiceImpl.class);

	/** The repository. */
	@Autowired
	private TargetConfigRepository repository;

	/** The target responses.*/
	@Autowired
	private TargetResponseRepository targetResponses;

	/** The target function codes.*/
	@Autowired
	private TargetFunctionCodeRepository targetFunctionCodes;


	/** The model mapper. */
	@Autowired
	ModelMapper modelMapper;

	/** The message source. */
	@Autowired
	MessageSource messageSource;

	/**
	 * Adds the target config.
	 *
	 * @param model
	 *            the model
	 * @return the target config dto
	 */
	@Override
	public TargetConfigDto addTargetConfig(TargetConfigDto model) {
		LOGGER.debug("TargetConfigServiceImpl >>> addTargetConfig() ====start");
		TargetConfigEntity entity = modelMapper.map(model, TargetConfigEntity.class);
		TargetConfigEntity targetEntity = repository.save(entity);
		TargetConfigDto targetModel = modelMapper.map(targetEntity, TargetConfigDto.class);
		LOGGER.debug("TargetConfigServiceImpl >>> addTargetConfig() ====ended");
		return targetModel;
	}

	/**
	 * Update target config.
	 *
	 * @param model
	 *            the model
	 * @return the target config dto
	 */
	@Override
	public TargetConfigDto updateTargetConfig(TargetConfigDto model) {
		LOGGER.debug("TargetConfigServiceImpl >>> updateTargetConfig() ====started");
		TargetConfigEntity entity = modelMapper.map(model, TargetConfigEntity.class);
		TargetConfigEntity targetEntity = repository.save(entity);
		TargetConfigDto targetModel = modelMapper.map(targetEntity, TargetConfigDto.class);
		LOGGER.debug("TargetConfigServiceImpl >>> updateTargetConfig() ====ended");
		return targetModel;
	}

	/**
	 * Delete target config.
	 *
	 * @param model
	 *            the model
	 */
	@Override
	public void deleteTargetConfig(TargetConfigDto model) {
		LOGGER.debug("TargetConfigServiceImpl >>> deleteTargetConfig() ====started");
		TargetConfigEntity entity = modelMapper.map(model, TargetConfigEntity.class);
		repository.delete(entity);
		LOGGER.debug("TargetConfigServiceImpl >>> deleteTargetConfig() ====ended");

	}

	@Override
	public List<TargetConfigDto> getTargetConfigList() {
		LOGGER.debug("TargetConfigServiceImpl >>> getTargetConfigList() ====started");
		List<TargetConfigEntity> entities = repository.findAll();
		List<TargetConfigDto> responseModel = new ArrayList<>();
		for (TargetConfigEntity targetEntity : entities) {
			TargetConfigDto model = modelMapper.map(targetEntity, TargetConfigDto.class);
			responseModel.add(model);
		}
		LOGGER.debug("TargetConfigServiceImpl >>> getTargetConfigList() ====ended");
		return responseModel;
	}

	/**
	 * Gets the target config by name.
	 *
	 * @param targetName
	 *            the target name
	 * @return the target config by name
	 */
	@Override
	public TargetConfigDto getTargetConfigByName(String targetName) {
		LOGGER.debug("TargetConfigServiceImpl >>> getTargetConfigById() ====started");
		TargetConfigEntity entity = repository.findByTargetName(targetName);
		if (entity == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.name", new Object[] {
					targetName
			}, Locale.getDefault()));
		}
		TargetConfigDto model = modelMapper.map(entity, TargetConfigDto.class);
		LOGGER.debug("TargetConfigServiceImpl >>> getTargetConfigById() ====ended");
		return model;
	}

	@Override
	public TargetConfigDetailsDto getAllTargetConfig(String port) {
		LOGGER.info("----TargetConfigServiceImpl / getAllTargetConfig()------Started");
		TargetConfigEntity entity =repository.getTargetConfig(port);
		if(entity==null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.port", new Object[] {
					(port)

			},Locale.getDefault()));
		}	
		int id = entity.getId().intValue();
		List<TargetFunctionCodeEntity> codesEntity=targetFunctionCodes.getTargetFunctionCodeDetails(String.valueOf(id));
		List<TargetResponseEntity> responsesEntity=targetResponses.getTargetResponseDetails(String.valueOf(id));
		TargetConfigDetailsDto dto = TargetConfigDetailUtils.getTargetConfigDto(entity, codesEntity, responsesEntity);
		LOGGER.info("-----TargetConfigServiceImpl / getAllTargetConfig()------end");
		return dto;
	}

	@Override
	public TargetConfigDetailsDto saveTargetConfigDetails(TargetConfigDetailsDto model) {
    LOGGER.info("------TargetConfigServiceImpl / saveTargetConfigDetails()--------Started");
    TargetConfigDetailsDto targetDto=new TargetConfigDetailsDto();
    List<TargetResponseDto> targetResponseDto = new ArrayList<>();
    List<TargetFunctionCodeDto> targetFunctionCodesDto = new ArrayList<>();
    TargetConfigEntity entity = modelMapper.map(model.getTargetConfig(),TargetConfigEntity.class);
    TargetConfigEntity configEntity = repository.save(entity);
    TargetConfigDto configDto =modelMapper.map(configEntity, TargetConfigDto.class);
    List<TargetResponseDto> responseDto=model.getTargetResponse();
    for(TargetResponseDto responsesDto:responseDto) {
    TargetResponseEntity responseEntity=modelMapper.map(responsesDto,TargetResponseEntity.class);
    responseEntity.setCreatedat(new Date());
    TargetResponseEntity targetEntity =targetResponses.save(responseEntity);
    TargetResponseDto dto = modelMapper.map(targetEntity,TargetResponseDto.class);
    targetResponseDto.add(dto);	
    }
    List<TargetFunctionCodeDto> codesDto =model.getTargetFunctionCodes();
    for(TargetFunctionCodeDto functionCodesDto:codesDto){
    	TargetFunctionCodeEntity targetEntity = modelMapper.map(functionCodesDto, TargetFunctionCodeEntity.class);
    	targetEntity.setCreatedat(new Date());
    	TargetFunctionCodeEntity functionEntity=targetFunctionCodes.save(targetEntity);
    	TargetFunctionCodeDto targetfunctionDto = modelMapper.map(functionEntity,TargetFunctionCodeDto.class);
    	targetFunctionCodesDto.add(targetfunctionDto);	
    }
    targetDto.setTargetConfig(configDto);
    targetDto.setTargetResponse(targetResponseDto);
    targetDto.setTargetFunctionCodes(targetFunctionCodesDto);
    LOGGER.info("-------TargetConfigServiceImpl / saveTargetConfigDetails()-----end");
	return targetDto;
	}	

}









