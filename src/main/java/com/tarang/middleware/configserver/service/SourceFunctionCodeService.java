package com.tarang.middleware.configserver.service;

import java.util.List;

import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;

/**
 * The Interface SourceFunctionCodesService. 
 *
 */
public interface SourceFunctionCodeService {



	/**
	 * Adds the source function codes.
	 *
	 * @param source function model
	 *            the source function model
	 * @return the source function codes dto
	 */
	SourceFunctionCodeDto addSourceFunctionCodes(SourceFunctionCodeDto sourceFunctionModel);

	/**
	 * Gets the source by id.
	 *
	 * @param sourceId
	 *            the source id
	 * @return the source by id
	 */
	SourceFunctionCodeDto getBysourceId(String sourceId);


	/**
	 * Update source function model.
	 *
	 * @param source function model
	 *            the source function model
	 * @return the source function codes dto
	 */

	SourceFunctionCodeDto updateSourceFunctionCodes(SourceFunctionCodeDto sourceFunctionModel);



	/**
	 * Gets the source function codes list.
	 *
	 * @return the source function codes list
	 */
	List<SourceFunctionCodeDto> getSourceFunctionCodesList();





	/**
	 * Delete source function codes.
	 *
	 * @param source function model
	 *            the source function model
	 */


	void deleteSourceFunctionCodes(SourceFunctionCodeDto sourceFunctionModel);















}
