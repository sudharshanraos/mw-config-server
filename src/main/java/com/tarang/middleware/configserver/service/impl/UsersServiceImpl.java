package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarang.middleware.configserver.dto.UsersDto;
import com.tarang.middleware.configserver.entity.UsersEntity;
import com.tarang.middleware.configserver.repository.UsersRepository;
import com.tarang.middleware.configserver.service.UsersService;
@Component
public class UsersServiceImpl implements UsersService {

	/** The constant logger.*/
	private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);



	/** The repository.*/
	@Autowired
	private UsersRepository repository;


	/** The model mapper.*/
	@Autowired
	ModelMapper modelMapper;


	/** The message source.*/
	@Autowired
	MessageSource messageSource;



	/** 
	 * Add the user model.
	 * 
	 * @param the usermodel.
	 * 
	 * @return the user dto.
	 * 
	 */
	public UsersDto addUser(UsersDto userModel) {
		LOGGER.info("------UserServiceImpl / addUser()-----------Started");
		userModel.setCreatedAt(new Date());
		UsersEntity entity = modelMapper.map(userModel,UsersEntity.class );
		UsersEntity entities = repository.save(entity);
		UsersDto dto = modelMapper.map(entities,UsersDto.class );
		LOGGER.info("------UserServiceImpl / addUser()-------end");
		return dto;
	}

	/**
	 * Get the  id.
	 * 
	 * @param the  id.
	 * 
	 * @return the User dto.
	 *
	 */
	@Override
	public UsersDto getByUuid( String uuid) {
		LOGGER.info("-------UserServiceImpl / getByUuid()--------------Started");
		UsersEntity entity=repository.findByUuid(uuid);
		if(entity==null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.uuid", new Object[] {
					Integer.valueOf(uuid)
			}, Locale.getDefault()));		
		}
		UsersDto dto = modelMapper.map(entity, UsersDto.class);
		LOGGER.info("-------UserServiceImpl / getByUuid()-------------end");
		return dto;
	}


	/**
	 * update user.
	 * 
	 * @param the user Model.
	 * 
	 * @return the user dto.
	 */
	@Override
	public UsersDto updateUser(UsersDto userModel) {
		userModel.setUpdatedAt(new Date());
		LOGGER.info("------UserServiceImpl / updateUser()---------Starting");
		UsersEntity entity =modelMapper.map(userModel,UsersEntity.class );
		UsersEntity entities=repository.save(entity);
		UsersDto dto=modelMapper.map(entities,UsersDto.class);
		LOGGER.info("--------UserServiceImpl / updateUser()-------end");
		return dto;
	}


	/**
	 * Get the user list.
	 * 
	 *@return the user list.
	 */

	/**
	 * Get the target function codes list.
	 * 
	 *@return the target function codes list.
	 */
	@Override
	public List<UsersDto> getUserList() {
		LOGGER.info("-------UserServiceImpl / getUserList()-----String");
		List<UsersEntity>entity=repository.findAll();
		List<UsersDto> dto = new ArrayList<>();
		for (UsersEntity userEntity : entity) {
			UsersDto model = modelMapper.map(userEntity, UsersDto.class);
			dto.add(model);
		}
		LOGGER.debug("SourceConfigServiceImpl >>> getSourceConfigList() ====ended");
		return dto;
	}



	/**
	 * delete the user dto.
	 * 
	 *@param userModel
	 *       the userModel.
	 */
	@Override
	public void deleteUserDto(UsersDto UserModel) {
		LOGGER.info("-------UserServiceImpl / deleteUserDto()-------------Started");
		UsersEntity entity = modelMapper.map(UserModel,UsersEntity.class);
		LOGGER.info("-------UserServiceImpl / deleteUserDto()--------------end");
		repository.delete(entity);

	}

	
	




}

