package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;


import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarang.middleware.configserver.dto.TargetResponseDto;
import com.tarang.middleware.configserver.entity.TargetResponseEntity;
import com.tarang.middleware.configserver.repository.TargetResponseRepository;
import com.tarang.middleware.configserver.service.TargetResponseService;

/**
 * The Class TargetResponseServiceImpl.
 * 
 * @author sudharshan.s
 */
@Component
public class TargetResponseServiceImpl implements TargetResponseService {


	/** The repository. */
	@Autowired
	private TargetResponseRepository repository;


	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TargetResponseService.class);

	/** The model mapper. */
	@Autowired
	ModelMapper modelMapper;

	/** The message source. */
	@Autowired
	MessageSource messageSource;

	/**
	 * Adds the target responses.
	 *
	 * @param targetModel
	 *            the target model
	 * @return the target responses dto
	 */
	@Override
	public TargetResponseDto addTargetResponses(TargetResponseDto targetModel) {
		LOGGER.info("------TargetResponsesServiceImpl /addTargetResponses()--------- Started");
		targetModel.setCreatedAt(new Date());
		TargetResponseEntity entity = modelMapper.map(targetModel, TargetResponseEntity.class);
		TargetResponseEntity tragetEntity = repository.save(entity);
		TargetResponseDto dto = modelMapper.map(tragetEntity, TargetResponseDto.class);
		LOGGER.info("------TargetResponsesServiceImpl /addTargetResponses()--------- end");
		return dto;
	}

	/**
	 * Gets the target by id.
	 *
	 * @param targetId
	 *            the target id
	 * @return the target by id
	 */
	@Override
	public List<TargetResponseDto>getTargetIdAndLanguageCode(String targetId,String languageCode) {
		LOGGER.info("------TargetResponsesServiceImpl /getTargetById()--------- Started");
		List<TargetResponseEntity> responsesEntity = repository.findByTargetIdAndLanguage(targetId, languageCode);
		List<TargetResponseDto> responseDto=new ArrayList<>();
		if (responsesEntity == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetId", new Object[] {
					Integer.valueOf(targetId)
			}, Locale.getDefault()));

		}
		for(TargetResponseEntity response:responsesEntity) {
			TargetResponseDto dto = modelMapper.map(response, TargetResponseDto.class);
			LOGGER.info("------TargetResponsesServiceImpl /findbyTargetId()--------- end");
			responseDto.add(dto);
		}
		return responseDto;


	}

	/**
	 * Update target responses.
	 *
	 * @param model
	 *            the model
	 * @return the target responses dto
	 */
	@Override
	public TargetResponseDto updateTargetResponses(TargetResponseDto model) {
		LOGGER.info("------TargetResponsesServiceImpl /updateTargetResponses()--------- Started");
		model.setUpdatedAt(new Date());
		TargetResponseEntity entity = modelMapper.map(model, TargetResponseEntity.class);
		TargetResponseEntity modelEntity = repository.save(entity);
		TargetResponseDto responseDto = modelMapper.map(modelEntity, TargetResponseDto.class);
		LOGGER.info("------TargetResponsesServiceImpl /updateTargetResponses()--------- end");
		return responseDto;
	}

	/**
	 * Gets the source responses list.
	 *
	 * @return the source responses list
	 */
	@Override
	public HashMap<String,List<TargetResponseDto>> getSourceResponsesList() {
		LOGGER.info("------TargetResponsesServiceImpl /getTargetResponsesList()--------- Started");
		HashMap<String,List<TargetResponseDto>> map=new HashMap<>();
		List<String> languages = repository.getLanguages();
		List<TargetResponseEntity> entities=repository.findAll();      
		for (String language : languages) {
			List<TargetResponseDto> responses = new ArrayList<>();	
			for(TargetResponseEntity targetEntity : entities) {
				if(language.equals(targetEntity.getLanguageCode())) {
					TargetResponseDto responseDto=modelMapper.map(targetEntity,TargetResponseDto.class);
					responses.add(responseDto);
				}
			}
			map.put(language, responses);
		}
		LOGGER.info("------TargetResponsesServiceImpl /getTargetResponsesList()--------- end");
		return map;
	}

	/**
	 * Delete target responses
	 *
	 * @param model
	 *            the model
	 */
	@Override
	public void deleteTargetResponses(TargetResponseDto model) {
		LOGGER.info("------TargetResponsesServiceImpl /deleteTargetResponses()--------- Started");
		TargetResponseEntity entity = modelMapper.map(model, TargetResponseEntity.class);
		repository.delete(entity);
	}


	@Override
	public TargetResponseDto getTargetById(String targetId) {
		TargetResponseEntity responsesentity = repository.findByTargetId(targetId);
		if (responsesentity == null) {       
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetId", new Object[] {
					Integer.valueOf(targetId)
			}, Locale.getDefault()));

		}
		TargetResponseDto dto = modelMapper.map(responsesentity, TargetResponseDto.class);
		return dto;

	}

}
