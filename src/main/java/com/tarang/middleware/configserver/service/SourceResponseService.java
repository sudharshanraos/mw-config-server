package com.tarang.middleware.configserver.service;

import java.util.List;

import com.tarang.middleware.configserver.dto.SourceResponseDto;

/**
 * The Interface SourceResponseService.
 * 
 * @author venkatramanap
 *
 */
public interface SourceResponseService {
	
	/**
	 * Adds the source response.
	 *
	 * @param resourcemodel the resource model
	 * @return the source response dto
	 */
	SourceResponseDto addSourceResponse(SourceResponseDto resourceModel);

	/**
	 * Gets the source by id.
	 *
	 * @param sourceId the source id
	 * @return the source by id
	 */
	
	 
	SourceResponseDto updateSourceResponse(SourceResponseDto model);

	/**
	 * Gets the source response list.
	 *
	 * @return the source response list
	 */
	List<SourceResponseDto> getSourceResponseList();
	
	/**
	 * Gets the source response by id.
	 *
	 * @param sourceid the source id
	 * @return the source response by id
	 */
	List<SourceResponseDto> getSourceIdAndLanguageCode (String sourceId,String languageCode);

	/**
	 * Delete source response.
	 *
	 * @param model the model
	 */
	void deleteSourceResponse(SourceResponseDto model);

	/**
	 * Gets the source by id.
	 *
	 * @param sourceId
	 *            the source id
	 * @return the source by id
	 */
	SourceResponseDto getSourceById(String sourceId);

	/**
	 * Gets the source response by id.
	 *
	 * @param sourceId
	 *            the source id
	 * @return the source response by id
	 */
	


}
