package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarang.middleware.configserver.dto.MerchantConfigDto;
import com.tarang.middleware.configserver.entity.MerchantConfig;
import com.tarang.middleware.configserver.repository.MerchantConfigRepository;
import com.tarang.middleware.configserver.service.MerchantConfigService;

/**
 * The Class MerchantConfigServiceImpl.
 * 
 * @author sudharshan.s
 */
@Component
public class MerchantConfigServiceImpl implements MerchantConfigService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantConfigServiceImpl.class);

    /** The repository. */
    @Autowired
    private MerchantConfigRepository repository;

    /** The model mapper. */
    @Autowired
    ModelMapper modelMapper;

    /** The message source. */
    @Autowired
    MessageSource messageSource;

    /**
     * Adds the merchant config.
     *
     * @param model
     *            the model
     * @return the merchant config dto
     */
    @Override
    public MerchantConfigDto addMerchantConfig(MerchantConfigDto model) {
        LOGGER.debug("MerchantConfigServiceImpl >>> addMerchantConfig() ====start");
        MerchantConfig entity = modelMapper.map(model, MerchantConfig.class);
        MerchantConfig merchantEntity = repository.save(entity);
        MerchantConfigDto merchantModel = modelMapper.map(merchantEntity, MerchantConfigDto.class);
        LOGGER.debug("MerchantConfigServiceImpl >>> addMerchantConfig() ====ended");
        return merchantModel;
    }

    /**
     * Gets the merchant config by id.
     *
     * @param merchantId
     *            the merchant id
     * @return the merchant config by id
     */
    public MerchantConfigDto getMerchantConfigById(String merchantId) {
        LOGGER.debug("MerchantConfigServiceImpl >>> getMerchantConfigById() ====started");
        MerchantConfig entity = repository.findByMerchantId(merchantId);
        if (entity == null) {
            throw new EntityNotFoundException(messageSource.getMessage("error.invalid.merchantId", new Object[] {
                    merchantId
            }, Locale.getDefault()));
        }
        MerchantConfigDto model = modelMapper.map(entity, MerchantConfigDto.class);
        LOGGER.debug("MerchantConfigServiceImpl >>> getMerchantConfigById() ====ended");
        return model;
    }

    /**
     * Update merchant config.
     *
     * @param model
     *            the model
     * @return the merchant config dto
     */
    @Override
    public MerchantConfigDto updateMerchantConfig(MerchantConfigDto model) {
        LOGGER.debug("MerchantConfigServiceImpl >>> updateMerchantConfig() ====started");
        MerchantConfig entity = modelMapper.map(model, MerchantConfig.class);
        MerchantConfig merchantEntity = repository.save(entity);
        MerchantConfigDto merchantModel = modelMapper.map(merchantEntity, MerchantConfigDto.class);
        LOGGER.debug("MerchantConfigServiceImpl >>> updateMerchantConfig() ====ended");
        return merchantModel;
    }

    @Override
    public List<MerchantConfigDto> getMerchantConfigList() {
        LOGGER.debug("MerchantConfigServiceImpl >>> getMerchantConfigList() ====started");
        List<MerchantConfig> entities = repository.findAll();
        List<MerchantConfigDto> responseModel = new ArrayList<>();
        for (MerchantConfig merchantEntity : entities) {
            MerchantConfigDto model = modelMapper.map(merchantEntity, MerchantConfigDto.class);
            responseModel.add(model);
        }
        LOGGER.debug("MerchantConfigServiceImpl >>> getMerchantConfigList() ====ended");
        return responseModel;
    }

    /**
     * Delete merchant config.
     *
     * @param model
     *            the model
     */
    @Override
    public void deleteMerchantConfig(MerchantConfigDto model) {
        LOGGER.debug("MerchantConfigServiceImpl >>> deleteMerchantConfig() ====started");
        MerchantConfig entity = modelMapper.map(model, MerchantConfig.class);
        repository.delete(entity);
        LOGGER.debug("MerchantConfigServiceImpl >>> deleteMerchantConfig() ====ended");
    }
}
