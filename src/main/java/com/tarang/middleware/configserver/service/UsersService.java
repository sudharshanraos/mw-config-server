package com.tarang.middleware.configserver.service;

import java.util.HashMap;
import java.util.List;

import com.tarang.middleware.configserver.dto.UsersDto;

/** The interface UserService.*/
public interface UsersService {


	/**
	 * Adds the user.
	 *
	 * @param user model
	 *            the user model.
	 *            
	 * @return the user dto.
	 */
	UsersDto addUser(UsersDto userModel);

	/**
	 * Get the id.
	 * 
	 * @param id
	 *        the id.
	 *        
	 * @return  by id.
	 */

	UsersDto getByUuid(String uuid);

	/**
	 * update user Model.
	 * 
	 * @param userModel
	 *        The user model.
	 *        
	 * @return userDto.
	 */

	UsersDto updateUser(UsersDto userModel);

	/** 
	 * 
	 * Gets the UserDto.
	 * 
	 * @return the UserDto.
	 */

	List<UsersDto> getUserList();



	/** 
	 * Delete the UserDto
	 * 
	 * @param UserModel
	 *         the UserModel
	 *         
	 */

	void deleteUserDto (UsersDto userModel);

	/**
	 * Get the  id.
	 * 
	 * @param the  id.
	 * 
	 * @return the User dto.
	 *
	 */
	




}
