
package com.tarang.middleware.configserver.service;

import java.util.HashMap;
import java.util.List;

import com.tarang.middleware.configserver.dto.TargetResponseDto;

/**
 * The Interface TargetResponsesService.
 */
public interface TargetResponseService {

    /**
     * Adds the target responses.
     *
     * @param targetmodel
     *            the target model
     * @return the target responses dto
     */
    TargetResponseDto addTargetResponses(TargetResponseDto targetModel);

    /**
     * Gets the target by id.
     *
     * @param targetId
     *            the target id
     * @return the target by id
     */
    TargetResponseDto getTargetById(String targetId);

    /**
     * Gets the source responses list.
     *
     * @return the source responses list
     */
    HashMap<String,List<TargetResponseDto>> getSourceResponsesList();

    /**
     * Delete target responses.
     *
     * @param model
     *            the model
     */
    void deleteTargetResponses(TargetResponseDto model);

    /**
     * Update target responses.
     *
     * @param model
     *            the model
     * @return the target responses dto
     */
    TargetResponseDto updateTargetResponses(TargetResponseDto model);
    
     /**
      * 
      * @param targetId
      * 
      * @param languageCode
      *        the languageCode
      *        
      * @return list<TargetResponsesDto>
      */
    List<TargetResponseDto> getTargetIdAndLanguageCode(String targetId,String languageCode);

}
