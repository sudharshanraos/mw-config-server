package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.tarang.middleware.configserver.dto.MessageConfigDto;
import com.tarang.middleware.configserver.entity.MessageConfigEntity;
import com.tarang.middleware.configserver.repository.MessageConfigRepository;
import com.tarang.middleware.configserver.service.MessageConfigService;

/**
 * The Class MessageConfigServiceImpl.
 * 
 * @author sudharshan.s
 */
@Service
public class MessageConfigServiceImpl implements MessageConfigService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConfigServiceImpl.class);

    /** The repository. */
    @Autowired
    private MessageConfigRepository repository;

    /** The model mapper. */
    @Autowired
    ModelMapper modelMapper;

    /** The message message. */
    @Autowired
    MessageSource messageMessage;

    /**
     * Adds the message config.
     *
     * @param model
     *            the model
     * @return the message config dto
     */
    @Override
    public MessageConfigDto addMessageConfig(MessageConfigDto model) {
        LOGGER.debug("MessageConfigServiceImpl >>> addMessageConfig() ====start");
        MessageConfigEntity entity = modelMapper.map(model, MessageConfigEntity.class);
        MessageConfigEntity msgEntity = repository.save(entity);
        MessageConfigDto messageModel = modelMapper.map(msgEntity, MessageConfigDto.class);
        LOGGER.debug("MessageConfigServiceImpl >>> addMessageConfig() ====ended");
        return messageModel;
    }

    /**
     * Update message config.
     *
     * @param model
     *            the model
     * @return the message config dto
     */
    @Override
    public MessageConfigDto updateMessageConfig(MessageConfigDto model) {
        LOGGER.debug("MessageConfigServiceImpl >>> updateMessageConfig() ====started");
        MessageConfigEntity entity = modelMapper.map(model, MessageConfigEntity.class);
        MessageConfigEntity msgEntity = repository.save(entity);
        MessageConfigDto messageModel = modelMapper.map(msgEntity, MessageConfigDto.class);
        LOGGER.debug("MessageConfigServiceImpl >>> updateMessageConfig() ====ended");
        return messageModel;
    }

    /**
     * Delete message config.
     *
     * @param model
     *            the model
     */
    @Override
    public void deleteMessageConfig(MessageConfigDto model) {
        LOGGER.debug("MessageConfigServiceImpl >>> deleteMessageConfig() ====started");
        MessageConfigEntity entity = modelMapper.map(model, MessageConfigEntity.class);
        repository.delete(entity);
        LOGGER.debug("MessageConfigServiceImpl >>> deleteMessageConfig() ====ended");

    }

    @Override
    public List<MessageConfigDto> getMessageConfigList() {
        LOGGER.debug("MessageConfigServiceImpl >>> getMessageConfigList() ====started");
        List<MessageConfigEntity> entities = repository.findAll();
        List<MessageConfigDto> responseModel = new ArrayList<>();
        for (MessageConfigEntity msgEntity : entities) {
            MessageConfigDto model = modelMapper.map(msgEntity, MessageConfigDto.class);
            responseModel.add(model);
        }
        LOGGER.debug("MessageConfigServiceImpl >>> getMessageConfigList() ====ended");
        return responseModel;
    }

}
