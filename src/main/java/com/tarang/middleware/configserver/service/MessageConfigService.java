package com.tarang.middleware.configserver.service;

import java.util.List;

import com.tarang.middleware.configserver.dto.MessageConfigDto;

/**
 * The Interface MessageConfigService.
 * 
 * @author sudharshan.s
 */
public interface MessageConfigService {

    /**
     * Adds the message config.
     *
     * @param model
     *            the model
     * @return the message config dto
     */
    MessageConfigDto addMessageConfig(MessageConfigDto model);

    /**
     * Update message config.
     *
     * @param mmm
     *            the mmm
     * @return the message config dto
     */
    MessageConfigDto updateMessageConfig(MessageConfigDto mmm);

    /**
     * Delete message config.
     *
     * @param model
     *            the model
     */
    void deleteMessageConfig(MessageConfigDto model);

    /**
     * Gets the message config list.
     *
     * @return the message config list
     */
    List<MessageConfigDto> getMessageConfigList();

}
