package com.tarang.middleware.configserver.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.tarang.middleware.configserver.dto.SourceResponseDto;
import com.tarang.middleware.configserver.entity.SourceResponseEntity;
import com.tarang.middleware.configserver.repository.SourceResponseRepository;
import com.tarang.middleware.configserver.service.SourceResponseService;

/**
 * The Class SourceResponseServicempl.
 * 
 * @author sudharshan.s
 */
@Component
public class SourceResponseServicempl implements SourceResponseService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceResponseService.class);

	/** The repository. */
	@Autowired
	private SourceResponseRepository repository;

	/** The model mapper. */
	@Autowired
	ModelMapper modelMapper;

	/** The message source. */
	@Autowired
	MessageSource messageSource;


	/**
	 * Adds the source response.
	 *
	 * @param responseModel
	 *            the responsemodel
	 * @return the source response dto
	 */
	@Override
	public SourceResponseDto addSourceResponse(SourceResponseDto responseModel) {
		LOGGER.debug("SourceResponseServiceImpl >>> addSourceResponse() ====start");
		responseModel.setCreatedAt(new Date());
		SourceResponseEntity entity = modelMapper.map(responseModel, SourceResponseEntity.class);
		SourceResponseEntity responseEntity = repository.save(entity);
		SourceResponseDto response = modelMapper.map(responseEntity, SourceResponseDto.class);
		LOGGER.debug("SourceResponseServiceImpl >>> addSourceResponse() ====end");

		return response;
	}

	/**
	 * Gets the source by id.
	 *
	 * @param sourceId
	 *            the source id
	 * @return the source by id
	 */
	@Override
	public SourceResponseDto getSourceById(String sourceId) {
		LOGGER.debug("SourceResponseServiceImpl >>> getSourceById() ====start");
		SourceResponseEntity responseEntity = repository.findBySourceId(sourceId);
		if (responseEntity == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.sourceId", new Object[] {
					String.valueOf(sourceId)
			}, Locale.getDefault()));
		}
		SourceResponseDto sourceModel = modelMapper.map(responseEntity, SourceResponseDto.class);
		LOGGER.debug("SourceResponseServiceImpl >>> getSourceById() ====end");
		return sourceModel;
	}

	/**
	 * Update source response.
	 *
	 * @param model
	 *            the model
	 * @return the source response dto
	 */
	@Override
	public SourceResponseDto updateSourceResponse(SourceResponseDto model) {
		LOGGER.debug("SourceResponseServiceImpl >>> updateSourceResponse() ====start");
		model.setUpdatedAt(new Date());
		SourceResponseEntity sourceModel = modelMapper.map(model, SourceResponseEntity.class);
		SourceResponseEntity responseEntity = repository.save(sourceModel);
		SourceResponseDto responseModel = modelMapper.map(responseEntity, SourceResponseDto.class);
		LOGGER.debug("SourceResponseServiceImpl >>> updateSourceResponse() ====end");
		return responseModel;
	}

	/**
	 * Gets the source response list.
	 *
	 * @return the source response list
	 */
	public List<SourceResponseDto> getSourceResponseList() {
		List<SourceResponseEntity> entity = repository.findAll();
		List<SourceResponseDto> sourceModel = new ArrayList<>();
		for (SourceResponseEntity sourceEntity : entity) {
			SourceResponseDto model = modelMapper.map(sourceEntity, SourceResponseDto.class);
			sourceModel.add(model);
		}

		return sourceModel;
	}

	/**
	 * Gets the source response by id.
	 *
	 * @param sourceId
	 *            the source id
	 * @return the source response by id
	 */
	@Override
	public List<SourceResponseDto> getSourceIdAndLanguageCode(String sourceId,String languageCode) {
		LOGGER.debug("SourceResponseServiceImpl >>> getSourceResponseById() ====start");
		List<SourceResponseEntity> entity= repository.findBySourceIdAndLanguage(sourceId, languageCode);
		List<SourceResponseDto> responseDto=new ArrayList<>();
		if (entity == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.Sourceid", new Object[] {
					sourceId
			}, Locale.getDefault()));

		}
		for(SourceResponseEntity response:entity) {
			SourceResponseDto model = modelMapper.map(response, SourceResponseDto.class);
			LOGGER.debug("SourceResponseServiceImpl >>> getSourceResponseById() ====end");
			responseDto.add(model);
		}
		return responseDto;
	}


	/**
	 * Delete source response.
	 *
	 * @param model
	 *            the model
	 */
	@Override
	public void deleteSourceResponse(SourceResponseDto model) {
		LOGGER.debug("SourceResponseServiceImpl >>> deleteSourceResponse() ====start");
		SourceResponseEntity entity = modelMapper.map(model, SourceResponseEntity.class);
		repository.delete(entity);
		LOGGER.debug("SourceResponseServiceImpl >>> deleteSourceResponse() ====end");
	}
}




