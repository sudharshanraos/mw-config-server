package com.tarang.middleware.configserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tarang.middleware.configserver.entity.MerchantConfig;

/**
 * The Interface MerchantConfigRepository.
 * 
 * @author sudharshan.s
 */
public interface MerchantConfigRepository extends JpaRepository<MerchantConfig, String> {

    /**
     * Find by merchant id.
     *
     * @param merchantId
     *            the merchant id
     * @return the merchant config entity
     */
    MerchantConfig findByMerchantId(String merchantId);
}
