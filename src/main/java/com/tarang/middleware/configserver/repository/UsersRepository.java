package com.tarang.middleware.configserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tarang.middleware.configserver.entity.UsersEntity;

public interface UsersRepository  extends JpaRepository<UsersEntity ,String>{
		/**
		 * @param Id
		 * @return
		 */
		@Query("select e from UsersEntity e where e.uuid =:uuid")
		UsersEntity findByUuid(String uuid);
		
	

	}


