package com.tarang.middleware.configserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tarang.middleware.configserver.entity.SourceFunctionCodeEntity;

/**
 * The Interface Source Function Codes Repository.
 * 
 * @author venkatramanap
 *
 */
public interface SourceFunctionCodeRepository extends JpaRepository<SourceFunctionCodeEntity,String>{

	/**
	 * @param sourceId
	 * @return
	 */
	@Query("select e from SourceFunctionCodeEntity e where e.sourceId=:sourceId")
	SourceFunctionCodeEntity findBySourceId(String sourceId);





    /**
     * @param id
     * @return source function codes entity
     */
    @Query("select e from SourceFunctionCodeEntity e where e.sourceId = :sourceId")
     List<SourceFunctionCodeEntity> getSourceFunctionCodeDetails(String sourceId);


}
