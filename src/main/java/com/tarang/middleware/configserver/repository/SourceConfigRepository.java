package com.tarang.middleware.configserver.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tarang.middleware.configserver.entity.SourceConfigEntity;

/**
 * The Interface SourceConfigRepository.
 * 
 * @author sudharshan.s
 */
public interface SourceConfigRepository extends JpaRepository<SourceConfigEntity, String> {

    /**
     * Find by source name.
     *
     * @param soruceName
     *            the soruce name
     * @return the source config
     */
    SourceConfigEntity findBySourceName(String soruceName);
    
    
    /**
     * @param port
     * @return source config entity
     */
    @Query("SELECT e FROM SourceConfigEntity e WHERE e.port = :port")
     SourceConfigEntity getSourceConfig(@Param("port") String port);
    
    
    
    /**
     * @param id
     * @return source config entity
     */
    @Query("select e from SourceConfigEntity e where e.id = :id")
    SourceConfigEntity getById(long id);
    

    
    
    
    
   
    

    
}
