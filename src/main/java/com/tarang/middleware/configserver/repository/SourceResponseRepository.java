package com.tarang.middleware.configserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tarang.middleware.configserver.entity.SourceResponseEntity;



/**
 * The Interface TargetResponsesRepository.
 * 
 * @author venkatramanap
 *
 */
/**
 * @author venkatramanap
 *
 */
public interface SourceResponseRepository extends JpaRepository<SourceResponseEntity,String> {



	/**
	 * Find by source id.
	 *
	 * @param sourceId
	 * 
	 *            the source id
	 * @return the Source Response Entity
	 */
	@Query("SELECT e FROM SourceResponseEntity e WHERE e.sourceId = :sourceId and e.languageCode= :languageCode")
	List<SourceResponseEntity> findBySourceIdAndLanguage(String sourceId,String languageCode );
	
	
	/**
	 * @param sourceId
	 * @return
	 */
	@Query("SELECT e FROM SourceResponseEntity e WHERE e.sourceId = :sourceId")
	SourceResponseEntity findBySourceId(String sourceId );

	/**
	 * Find by id and source id.
	 *
	 * @param sourceId
	 * 
	 *            the source id
	 *            
	 * @param id 
	 *         the id
	 *                 
	 * @return the Source Response Entity
	 */
	@Query("SELECT e FROM SourceResponseEntity e WHERE e.id =  :id and e.sourceId = :sourceId")
	SourceResponseEntity findByIdAndSourceId(int id,String sourceId);
	
	
	
	/**
	 * @param sourceId
	 * 
	 * @return source response entity
	 */
	@Query("select e from SourceResponseEntity e where e.sourceId = :sourceId")
	List<SourceResponseEntity> getSourceResponseDetails(String sourceId);
	
	
	
}
