package com.tarang.middleware.configserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.tarang.middleware.configserver.entity.TargetConfigEntity;

/**
 * The Interface TargetConfigRepository.
 * 
 * @author sudharshan.s
 */
public interface TargetConfigRepository extends JpaRepository<TargetConfigEntity, String> {

    /**
     * Find by target name.
     *
     * @param targetName
     *            the target name
     * @return the target config
     */
    TargetConfigEntity findByTargetName(String targetName);
    
    
    /**
     * @param port
     *      The port
     *      
     * @return target config entity
     */
    @Query("SELECT e FROM TargetConfigEntity e WHERE e.port = :port")
    TargetConfigEntity getTargetConfig (String port);

    
    /**
     * @param id
     * @return target config entity
     */
    @Query("select e from TargetConfigEntity e where e.id=:id")
    TargetConfigEntity getById(long id);
    
    
    
    
    
    
    
   
}
