package com.tarang.middleware.configserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tarang.middleware.configserver.entity.TargetFunctionCodeEntity;

public interface TargetFunctionCodeRepository extends JpaRepository<TargetFunctionCodeEntity ,String>{
	
	
	/**
	 * @param Id
	 * @return
	 */
	@Query("select e from TargetFunctionCodeEntity e where e.id=:id")
	TargetFunctionCodeEntity findById(int id);
	
	
	/**
	 * @return get languages
	 */
	@Query("select distinct e.languageCode from TargetFunctionCodeEntity e")
	List<String> getLanguages();
	

	/**
	 * @param id
	 * @param languageCode
	 * @return TargetFunctionCodesEntity
	 */
	@Query("select e from TargetFunctionCodeEntity e where e.id=:id and e.languageCode=:languageCode")
	List<TargetFunctionCodeEntity> findByIdAndLanguageCode(int id ,String languageCode);
	
	
	/**
	 * @param targetId
	 * 
	 * @return  target function codes entity
	 */
	@Query("select e from TargetFunctionCodeEntity  e where e.targetID = :targetId")
	List<TargetFunctionCodeEntity> getTargetFunctionCodeDetails(String targetId);
	

}
