package com.tarang.middleware.configserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tarang.middleware.configserver.entity.SourceMessageMappingEntity;

public interface SourceMessageMappingRepository extends JpaRepository<SourceMessageMappingEntity, Long>{
    
      /**
     * @param sourceId
     * 
     * @return  get message id
     */
    @Query("SELECT e.messageId FROM SourceMessageMappingEntity e WHERE e.sourceId = :sourceId")
       Long getMessageID(@Param("sourceId") String sourceId);
    
      
      /**
     * @param sourceId
     * 
     * @return source message detail entity
     */
    @Query("select e from SourceMessageMappingEntity e where e.sourceId = :sourceId")
    List<SourceMessageMappingEntity> getSourceMessageMapping(String sourceId);

}
