package com.tarang.middleware.configserver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.tarang.middleware.configserver.entity.TargetResponseEntity;

/**
 * The Interface TargetResponseRepository.
 * 
 * @author venkatramanap.
 *
 */
public interface TargetResponseRepository extends JpaRepository<TargetResponseEntity,String> {


	/**
	 * @param targetId
	 * @return
	 */
	@Query("SELECT e from TargetResponseEntity e WHERE e.targetId=:targetId")
	TargetResponseEntity findByTargetId(String targetId);


	/**
	 * @param targetId
	 * @param languageCode
	 * @return
	 */
	@Query("SELECT e FROM TargetResponseEntity e WHERE e.targetId = :targetId and e.languageCode = :languageCode")
	List<TargetResponseEntity> findByTargetIdAndLanguage(String targetId,String languageCode );




	/**
	 * @param languageCode
	 * 
	 * @return list
	 */
	@Query ("SELECT DISTINCT e.languageCode FROM TargetResponseEntity e")
	List<String> getLanguages();
	
	
	
	
	
	/**
	 * @param targetId
	 * 
	 * @return target response entity
	 */
	@Query("select e from TargetResponseEntity e where e.targetId=:targetId")
	List<TargetResponseEntity> getTargetResponseDetails(String targetId);









}
