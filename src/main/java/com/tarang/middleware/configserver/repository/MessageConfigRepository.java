package com.tarang.middleware.configserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.tarang.middleware.configserver.entity.MessageConfigEntity;

/**
 * The Interface MessageConfigRepository.
 * 
 * @author sudharshan.s
 */
public interface MessageConfigRepository extends JpaRepository<MessageConfigEntity, Long>{
    
 
    @Query("SELECT e FROM MessageConfigEntity e WHERE e.id = :id")
    MessageConfigEntity getMessageConfig(@Param("id") Long id);
   
    
}
