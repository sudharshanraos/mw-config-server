package com.tarang.middleware.configserver.enums;

import lombok.Getter;

/**
 * APi Details.
 * 
 * @author sudharshan.s
 */
@Getter
public enum ApiDetails {

    /** The merchant config management. */
    MERCHANT_CONFIG_MANAGEMENT("Merchant Configuration Management", "Merchant Configuration Management API Services"),

    /** The message config management. */
    MESSAGE_CONFIG_MANAGEMENT("Message Configuration Management", "Message Configuration Management API Services"),

    /** The source config management. */
    SOURCE_CONFIG_MANAGEMENT("Source Configuration Management", "Source Configuration Management API Services"),

    /** The system config management. */
    SYSTEM_CONFIG_MANAGEMENT("System Configuration Management", "System Configuration Management API Services"),

    /** The target config management. */
    TARGET_CONFIG_MANAGEMENT("Target Configuration Management", "Target Configuration Management API Services"),
    
    SOURCE_RESPONSE_MANAGEMENT("Source Responses Management", "Source Responses Management API Services"),

    ;

    /** The key. */
    private final String key;

    /** The value. */
    private final String value;

    /**
     * A mapping between the integer code and its corresponding text to
     * facilitate lookup by code.
     *
     * @param key
     *            the key
     * @param value
     *            the value
     */

    ApiDetails(String key, String value) {
        this.value = value;
        this.key = key;
    }
    
}
