package com.tarang.middleware.configserver.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.SourceFunctionCodeDto;
import com.tarang.middleware.configserver.service.SourceFunctionCodeService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class SourceFunctionCodesController.
 * 
 * @author venkatramanap
 *
 */
@RestController
@RequestMapping(ApiConstants.SOURCE_FUNCTION_CODE_PARENT)
@Api(value=ApiConstants.SOURCE_FUNCTION_CODE_MANAGEMENT,tags={
		ApiConstants.SOURCE_FUNCTION_CODE_MANAGEMENT
})
public class SourceFunctionCodeController<T> {

	/** The Constant Logger.*/
	private static final Logger LOGGER= LoggerFactory.getLogger(SourceFunctionCodeController.class);


	/** The Source Function Codes Service.*/
	@Autowired
	private SourceFunctionCodeService sourceFunctionCodesService;


	/** The responseCreator.*/
	@Autowired
	private GeneralResponseCreator responseCreator;


	/** The messageSource.*/
	@Autowired
	private MessageSource messageSource;

	/**
	 * Add target responses.
	 *
	 * @param sourceFunctionModel
	 *            the sourceFunctionModel
	 * @return the general response
	 * 
	 */

	@ApiOperation(value = "${swgr.op.source.add.value}", notes = "${swgr.op.source.add.note}")
	@PostMapping(path=ApiConstants.SOURCE_FUNCTION_CODE_ADD_CONFIG,
	consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<SourceFunctionCodeDto> addSourceFunctionCodes(
			@ApiParam(required = true, value = "${swgr.an.source.add.value}")@RequestBody SourceFunctionCodeDto sourceFunctionModel){
		SourceFunctionCodeDto codesDto=sourceFunctionCodesService.addSourceFunctionCodes(sourceFunctionModel);
		LOGGER.info("------SourceFunctionCodesController / addSourceFunctionCodes()----------Started");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, codesDto);

	}


	/**
	 * Update source function codes.
	 *
	 * @param sourceId
	 *            the source id
	 * @param sourceFunctionCodes
	 *            the sourceFunctionCodes
	 * @return the general response
	 */

	@PostMapping(path=ApiConstants.UPDATE_SOURCE_FUNCTION_CODE,
			consumes =org.springframework.http.MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<SourceFunctionCodeDto>updateSourceFunctionCodes(
			@ApiParam(value = "${swagger.param.sourceId}", required = true) @RequestParam String sourceId,
			@Validated @RequestBody SourceFunctionCodeDto sourceFunctionModel){
		SourceFunctionCodeDto dto=sourceFunctionCodesService.getBysourceId(sourceId);
		LOGGER.info("-----SourceFunctionCodesController / updateSourceFunctionCodes()-------Started");
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.sourceId", new Object[] {
					Integer.valueOf(sourceId)
			}, Locale.getDefault()));
		}
		SourceFunctionCodeDto codesDto = sourceFunctionCodesService.updateSourceFunctionCodes(sourceFunctionModel);
		LOGGER.info("----SourceFunctionCodesController / updateSourceFunctionCodes()-----end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, codesDto);

	}

	/**
	 * Gets the source function codes list.
	 *
	 * @return the source function codes list.
	 */

	@GetMapping (path = ApiConstants.SOURCE_FUNCTION_CODE_LIST,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<List<SourceFunctionCodeDto>> getSourceFunctionCodesList(){
		List<SourceFunctionCodeDto> functionCodesDto=sourceFunctionCodesService.getSourceFunctionCodesList();
		LOGGER.info("-----SourceFunctionCodesController / getSourceFunctionCodesList()-------Started");

		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, functionCodesDto);	
	}


//	/**
//	 * Gets the source function codes by id.
//	 *
//	 * @param sourceId
//	 *            the source id
//	 * @return the source function codes by id
//	 */
//	@GetMapping(path = ApiConstants.SOURCE_FUNCTION_CODES_LIST_BY_ID,produces=MediaType.APPLICATION_JSON_VALUE)
//	@ResponseStatus(HttpStatus.OK)
//	public GeneralResponse<List<SourceFunctionCodesDto>>getSourceFunctionCodesById(
//			@ApiParam(value = "${swagger.param.sourceid}", required = true) @RequestParam int sourceId,String languageCode) {
//		LOGGER.info("-----SourceFunctionCodesController / getSourceFunctionCodesById()-------Started");
//
//		List<SourceFunctionCodesDto> dto =sourceFunctionCodesService.getBySourceIdAndLanguageCode(sourceId, languageCode);
//		if (dto == null) {
//			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetId", new Object[] {
//					sourceId
//			}, Locale.getDefault()));
//		}
//		LOGGER.info("-----SourceFunctionCodesController / getSourceFunctionCodesById()-------end");
//
//		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, dto);	
//
//	}




	/**
	 * Delete Source Function Codes .
	 *
	 * @param sourceId
	 *            the source id
	 * @return the general response
	 */
	@DeleteMapping(path=ApiConstants.SOURCE_FUNCTION_CODE_DELETE,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<SourceFunctionCodeDto>deleteSourceFunctionCodes(
			@ApiParam(value = "${swagger.param.sourcetid}", required = true) @RequestParam String sourceId) {
		LOGGER.info("-----SourceFunctionCodesController / deleteSourceFunctionCodes()-------Started");

		SourceFunctionCodeDto dto=sourceFunctionCodesService.getBysourceId(sourceId);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.sourceid", new Object[] {
					sourceId
			}, Locale.getDefault()));
		}
		sourceFunctionCodesService.deleteSourceFunctionCodes(dto);
		LOGGER.info("-----SourceFunctionCodesController / deleteSourceFunctionCodes()-------end");

		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, dto);	


	}


}
