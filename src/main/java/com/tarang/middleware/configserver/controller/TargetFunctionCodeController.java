package com.tarang.middleware.configserver.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.TargetFunctionCodeDto;
import com.tarang.middleware.configserver.service.TargetFunctionCodeService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

	
	
	
	/**
	 *The Class TargetFunctionCodesController. 
	 * 
	 * @author venkatramanap
	 * 
	 */
	@RestController
	@RequestMapping(path=ApiConstants.TARGET_FUNCTION_CODE_ADD_PARENT)
	@Api(value=ApiConstants.TARGET_FUNCTION_CODE_MANAGEMENT,tags= {
			ApiConstants.TARGET_FUNCTION_CODE_MANAGEMENT
	})
	public class TargetFunctionCodeController<T> {
		
		
		
		/** The constant logger.*/
		private static final Logger LOGGER=LoggerFactory.getLogger(TargetFunctionCodeController.class);
		
		
		/** The codes service.*/
		@Autowired
		private TargetFunctionCodeService codesService;
		
		
		/** The response creator.*/
		@Autowired
		private GeneralResponseCreator responseCreator;
		
		
		/** The message source.*/
		@Autowired
		private MessageSource messageSource;
/**
 * Add target function codes.
 * 
 * @param targetFunctionModel
 *        the targetFunctionModel.
 *        
 * @return the target function codes dto.
 */
		
		@ApiOperation(value = "${swgr.op.target.add.value}", notes = "${swgr.op.target.add.note}")
		@PostMapping(path=ApiConstants.TARGET_FUNCTION_CODE_ADD_CONFIG,consumes=MediaType.APPLICATION_JSON_VALUE)
		@ResponseStatus(HttpStatus.OK)
		public GeneralResponse<TargetFunctionCodeDto>addTargetFunctionCodes(
			@ApiParam(required=true, value = "${swgr.op.target.add.value}") @RequestBody TargetFunctionCodeDto targetFunctionModel){
		    LOGGER.info("--------TargetFunctionCodesController / addTargetFunctionCodes()--------Started");
	    TargetFunctionCodeDto codesDto=codesService.addTargetFunctionCodes(targetFunctionModel);
	    LOGGER.info("--------TargetFunctionCodesController / addTargetFunctionCodes()--------end");
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, codesDto);
				
		}
		
		/**
		 * update the target function codes.
		 * 
		 * @param id
		 *       The id.
		 *       
		 * @param targetFunctionModel      
		 *        The targetFunctionModel.
		 *        
		 * @return the target function codes dto.
		 */
	
		@PostMapping(path=ApiConstants.UPDATE_TARGET_FUNCTION_CODE,
		consumes=MediaType.APPLICATION_JSON_VALUE ,produces=MediaType.APPLICATION_JSON_VALUE)
		@ResponseStatus(HttpStatus.OK)
		public GeneralResponse<TargetFunctionCodeDto>updateTargetFunctionCodes(
				@ApiParam(value = "${swgr.op.target.add.value}",required = true)   @RequestParam  int id ,@Validated
				@RequestBody TargetFunctionCodeDto targetFunctionModel){
		TargetFunctionCodeDto dto=codesService.getById(id);
		LOGGER.info("-----TargetFunctionCodesController / updateTargetFunctionCodes()---------Started");
		if(dto==null) {
			throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_ID , new Object[] {
					Integer.valueOf(id)
			}, Locale.getDefault()));
		}		
		TargetFunctionCodeDto codesDto=codesService.updateFunctionCodes(targetFunctionModel);
		LOGGER.info("--------TargetFunctionCodesController / updateTargetFunctionCodes()----------end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, codesDto);
			
		}
		
		/**
		 * Get the target function codes list.
		 * 
		 * @return the Target function codes list.
		 */
		
		@GetMapping(path=ApiConstants.TARGET_FUNCTION_CODES_LIST,produces=MediaType.APPLICATION_JSON_VALUE)
		@ResponseStatus(HttpStatus.OK)
		public GeneralResponse<HashMap<String,List<TargetFunctionCodeDto>>>getTargetFunctionCodesList() {
			LOGGER.info("-------TargetFunctionCodesController / getTargetFunctionCodesList()------Started");
			HashMap<String,List<TargetFunctionCodeDto>> codesDto = codesService.getTargetFunctionCodesList();
            LOGGER.info("-------TargetFunctionCodesController / getTargetFunctionCodesList()------end");
return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS,true,codesDto);
		}
		
		
		/** 
		 * Get the target by id.
		 * 	
		 * @param targetId and languageCode	
		 *       The targetId and languageCode.
		 * 
		 * @return the target function codes dto.
		 */
	
		@GetMapping(path=ApiConstants.TARGET_FUNCTION_CODES_LIST_BY_ID, produces=MediaType.APPLICATION_JSON_VALUE)
		@ResponseStatus(HttpStatus.OK)
		public GeneralResponse<List<TargetFunctionCodeDto>> getTargetFunctionCodesById(
		@ApiParam(value="${swgr.param.id}" ,required=true)
		@RequestParam int id ,String languageCode){ 
			LOGGER.info("-------TargetFunctionCodesController / getTargetFunctionCodesById()---------Started");
			List<TargetFunctionCodeDto> dto = codesService.getByTargetIdAndLanguageCode(id,languageCode);
			if(dto==null) {
				throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_ID, new Object[] {
						(id)
				},Locale.getDefault()));
			}
			LOGGER.info("-----TargetFunctionCodesController / getTargetFunctionCodesById()---------end");
			return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true,dto);
		}
		
						
		/** 
		 * delete the target function codes.
		 * 
		 * @param the id.
		 * 
		 * @return the target function codes dto.
		 */
				
		@DeleteMapping(path=ApiConstants.TARGET_FUNCTION_CODES_DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
		@ResponseStatus(HttpStatus.OK)
		public GeneralResponse<TargetFunctionCodeDto>deleteTargetFunctionCodes(
				@ApiParam(value="${swgr.param.id)" , required=true)@RequestParam int id){
			LOGGER.info("--------TargetFunctionCodesController / deleteTargetFunctionCodes()---------Starting");
			TargetFunctionCodeDto dto=codesService.getById(id);
			if(dto==null) {
				throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_ID, new Object[] {
						(id)
					},Locale.getDefault()));
			}
			codesService.deleteTargetFunctionCodesDto(dto);
			LOGGER.info("-----TargetFunctionCodesController / deleteTargetFunctionCodes()-----end");
			return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true,dto);		
		
		}				
		}
