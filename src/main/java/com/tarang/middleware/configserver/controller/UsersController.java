package com.tarang.middleware.configserver.controller;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.UsersDto;
import com.tarang.middleware.configserver.service.UsersService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(path=ApiConstants.USERS_ADD_PARENT)
@Api(value=ApiConstants.USERS_MANAGEMENT,tags= {
		ApiConstants.USERS_MANAGEMENT
})
public class UsersController {




	/** The constant logger.*/
	private static final Logger LOGGER=LoggerFactory.getLogger(UsersController.class);


	/** The codes service.*/
	@Autowired
	private UsersService service;


	/** The response creator.*/
	@Autowired
	private GeneralResponseCreator responseCreator;


	/** The message source.*/
	@Autowired
	private MessageSource messageSource;
	/**
	 * Add user.
	 * 
	 * @param userModel
	 *        the userModel.
	 *        
	 * @return the user dto.
	 */

	@ApiOperation(value = "${swgr.op.user.add.value}", notes = "${swgr.op.user.add.note}")
	@PostMapping(path=ApiConstants.USERS_ADD_CONFIG,consumes=MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<UsersDto>addUser(
			@ApiParam(required=true, value = "${swgr.op.user.add.value}") @RequestBody UsersDto userModel){
		LOGGER.info("--------UserController / addUser()--------Started");
		UsersDto dto=service.addUser(userModel);
		LOGGER.info("--------UserController / addUser()--------end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true,dto);

	}

	/**
	 * update the user.
	 * 
	 * @param id
	 *       The id.
	 *       
	 * @param userModel      
	 *        The userModel.
	 *        
	 * @return the user dto.
	 */

	@PostMapping(path=ApiConstants.UPDATE_USERS,
			consumes=MediaType.APPLICATION_JSON_VALUE ,produces=MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<UsersDto>updateuUser(
			@ApiParam(value = "${swgr.op.user.add.value}",required = true)   @RequestParam String uuid ,@Validated
			@RequestBody UsersDto userModel){
		UsersDto dto=service.getByUuid(uuid);
		LOGGER.info("-----UserController / updateUser()---------Started");
		if(dto==null) {
			throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_UUID , new Object[] {
					Integer.valueOf(uuid)
			}, Locale.getDefault()));
		}		
		UsersDto userDto=service.updateUser(userModel);
		LOGGER.info("--------UserController / updateUser()----------end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, userDto);

	}

	/**
	 * Get the user list.
	 * 
	 * @return the user list.
	 */

	@GetMapping(path=ApiConstants.USERS_LIST,produces=MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<List<UsersDto>> getUserList() {
		LOGGER.info("-------UserController / getUserList()------Started");
		List<UsersDto> dto= service.getUserList();
		LOGGER.info("-------UserController / getUserList()------end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS,true,dto);
	}




	/** 
	 * delete the target function codes.
	 * 
	 * @param the id.
	 * 
	 * @return the target function codes dto.
	 */

	@DeleteMapping(path=ApiConstants.USERS_DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<UsersDto>deleteUser(
			@ApiParam(value="${swgr.param.id)" , required=true)@RequestParam String uuid){
		LOGGER.info("--------UserController / deleteUser()---------Starting");
		UsersDto dto=service.getByUuid(uuid);
		if(dto==null) {
			throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_UUID, new Object[] {
					(uuid)
			},Locale.getDefault()));
		}
		service.deleteUserDto(dto);
		LOGGER.info("-----UserController / deleteUser()-----end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true,dto);		

	}				
}


