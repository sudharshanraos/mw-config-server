package com.tarang.middleware.configserver.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.MessageConfigDto;
import com.tarang.middleware.configserver.service.MessageConfigService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class MessageConfigController.
 *
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.MESSAGE_PARENT)
@Api(value = ApiConstants.MESSAGE_CONFIG_MANAGEMENT, tags = {
        ApiConstants.MESSAGE_CONFIG_MANAGEMENT
})
@SuppressWarnings("unchecked")
public class MessageConfigController<T> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConfigController.class);

    /** The message service. */
    @Autowired
    private MessageConfigService messageService;

    /** The response creator. */
    @Autowired
    private GeneralResponseCreator responseCreator;

    /**
     * Adds the message config.
     *
     * @param model
     *            the model
     * @return the general response
     */
    @ApiOperation(value = "${swgr.op.merchant.add.value}", notes = "${swgr.op.merchant.add.note}")
    @PostMapping(path = ApiConstants.MESSAGE_ADD_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GeneralResponse<T> addMessageConfig(
            @ApiParam(required = true, value = "${swgr.an.merchant.update.value}") @RequestBody MessageConfigDto model) {
        LOGGER.info("-------MessageConfigController /addMessageConfig()--------- Started");
        MessageConfigDto messageDto = messageService.addMessageConfig(model);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) messageDto);
    }

    /**
     * Update message config.
     *
     * @param MessageName
     *            the message name
     * @param model
     *            the model
     * @return the general response
     */
    @PostMapping(path = ApiConstants.MESSAGE_UPDATE_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
    public GeneralResponse<T> updateMessageConfig(@ApiParam(value = "${swagger.param.merchantId}", required = true) @RequestParam int id,
            @Validated @RequestBody MessageConfigDto model) {
        LOGGER.info("-------MessageConfigController /updateMessageConfig()--------- Started");
        MessageConfigDto updatedMessage = messageService.updateMessageConfig(model);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) updatedMessage);
    }

    /**
     * Gets the message config list.
     *
     * @return the message config list
     */
    @GetMapping(path = ApiConstants.MESSAGE_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
    public GeneralResponse<T> getMessageConfigList() {
        LOGGER.info("-------MessageConfigController /getMessageConfigList()--------- Started");
        List<MessageConfigDto> msg = messageService.getMessageConfigList();
        LOGGER.info("-------MessageConfigController /getMessageConfigList()--------- end ");
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) msg);
    }

}
