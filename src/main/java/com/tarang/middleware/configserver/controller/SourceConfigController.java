package com.tarang.middleware.configserver.controller;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.SourceConfigDetailsDto;
import com.tarang.middleware.configserver.dto.SourceConfigDto;
import com.tarang.middleware.configserver.dto.SourceDetails;
import com.tarang.middleware.configserver.service.SourceConfigService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class SourceConfigController.
 *
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.SOURCE_PARENT)
@Api(value = ApiConstants.SOURCE_CONFIG_MANAGEMENT, tags = {
		ApiConstants.SOURCE_CONFIG_MANAGEMENT
})
@SuppressWarnings("unchecked")
public class SourceConfigController<T> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SourceConfigController.class);

	/** The source service. */
	@Autowired
	private SourceConfigService sourceService;

	/** The response creator. */
	@Autowired
	private GeneralResponseCreator responseCreator;

	/** The message source. */
	@Autowired
	private MessageSource messageSource;

	/**
	 * Adds the source config.
	 *
	 * @param model
	 *            the model
	 * @return the general response
	 */
	@ApiOperation(value = "${swgr.op.merchant.add.value}", notes = "${swgr.op.merchant.add.note}")
	@PostMapping(path = ApiConstants.SOURCE_ADD_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> addSourceConfig(
			@ApiParam(required = true, value = "${swgr.an.merchant.update.value}") @RequestBody SourceConfigDto model) {
		LOGGER.info("-------SourceConfigController /addSourceConfig()--------- Started");
		SourceConfigDto sourceDto = sourceService.addSourceConfig(model);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) sourceDto);
	}

	/**
	 * Update source config.
	 *
	 * @param targetName
	 *            the target name
	 * @param model
	 *            the model
	 * @return the general response
	 */
	@PostMapping(path = ApiConstants.SOURCE_UPDATE_CONFIG,consumes = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> updateSourceConfig(@ApiParam(value = "${swagger.param.merchantId}", required = true) @RequestParam String sourceName,
			@Validated @RequestBody SourceConfigDto model) {
		LOGGER.info("-------SourceConfigController /updateSourceConfig()--------- Started");
		SourceConfigDto target = sourceService.getSourceConfigByName(sourceName);
		if (target == null) {
			throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_SOURCE_NAME, new Object[] {
					sourceName.toString()
			}, Locale.getDefault()));
		}
		SourceConfigDto updatedSource = sourceService.updateSourceConfig(model);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) updatedSource);
	}

	/**
	 * Gets the source config list.
	 *
	 * @return the source config list
	 */
	@GetMapping(path = ApiConstants.SOURCE_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> getSourceConfigList() {
		LOGGER.info("-------SourceConfigController /getSourceConfigList()--------- Started");
		List<SourceConfigDto> source = sourceService.getSourceConfigList();
		LOGGER.info("-------SourceConfigController /getSourceConfigList()--------- end ");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) source);
	}

	/**
	 * Gets the source config by name.
	 *
	 * @param targetName
	 *            the target name
	 * @return the source config by name
	 */
	@GetMapping(path = ApiConstants.SOURCE_LIST_BY_ID,produces = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> getSourceConfigByName(
			@ApiParam(required = true, value = "${swagger.param.merchantId}") @RequestParam String sourceName) {
		LOGGER.info("-------SourceConfigController /getSourceConfigByName()--------- started");
		SourceConfigDto dto = sourceService.getSourceConfigByName(sourceName);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.sourceName", new Object[] {
					sourceName
			}, Locale.getDefault()));
		}
		LOGGER.info("-------SourceConfigController /getSourceConfigByName()--------- end ");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) dto);
	}

	/**
	 * Delete source config.
	 *
	 * @param targetName
	 *            the target name
	 * @return the general response
	 */
	@DeleteMapping(path = ApiConstants.SOURCE_DELETE_CONFIG, produces = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> deleteSourceConfig(@ApiParam(value = "${swagger.param.sourceName}", required = true) @RequestParam String sourceName) {
		LOGGER.info("-------SourceConfigController /deleteSourceConfig()--------- started");
		SourceConfigDto dto = sourceService.getSourceConfigByName(sourceName);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_SOURCE_NAME, new Object[] {
					sourceName
			}, Locale.getDefault()));
		}
		sourceService.deleteSourceConfig(dto);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) dto);
	}
	/**
	 * Gets the source msg details.
	 *
	 * @param port
	 *            the port
	 * @return the source msg details
	 */
	@GetMapping(path = ApiConstants.GET_SOURCE_MSG_DETAILS,produces = MediaType.APPLICATION_JSON_VALUE)
	public SourceDetails getSourceMsgDetails(@ApiParam(value = "${swagger.param.port}", required = true) @RequestParam String port) {
		LOGGER.info("-------SourceConfigController /getSourceMsgDetails()--------- started");
		SourceDetails sourceDetails = sourceService.getSourceDetails(port);
		LOGGER.info("-------SourceConfigController /getSourceConfigList()--------- end ");
		return sourceDetails;
	}

    @GetMapping(path=ApiConstants.GET_SOURCE_CONFIG,produces=MediaType.APPLICATION_JSON_VALUE)
	public SourceConfigDetailsDto getAllSourceConfig(@ApiParam(value="${swagger.param.port}", required=true)@RequestParam String port){
		LOGGER.info("-----SourceConfigController / getAllSourceConfig()--------Started");
		SourceConfigDetailsDto dto=sourceService.getAllSourceConfig(port);
		LOGGER.info("------SourceConfigController / getAllSourceConfig()------end");
		return dto;

	}
    
    
        
    @PostMapping(path = ApiConstants.SAVE_SOURCE_DETAILS,produces = MediaType.APPLICATION_JSON_VALUE)
    public SourceConfigDetailsDto saveSourceConfig(@ApiParam(required = true, value = "${swgr.an.merchant.update.value}") 
    @RequestBody SourceConfigDetailsDto sourceDto){ 
        LOGGER.info("----SourceConfigController / getSourceConfigDetails()-------Started");
        SourceConfigDetailsDto sourceDetails=sourceService.saveSourceConfigDetails(sourceDto);
        LOGGER.info("------SourceConfigController / getSourceConfigDetails()-------end");
        return sourceDetails;
        
        
    }

}
