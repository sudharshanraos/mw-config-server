package com.tarang.middleware.configserver.controller;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.MerchantConfigDto;
import com.tarang.middleware.configserver.service.MerchantConfigService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class MerchantConfigController.
 *
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.MERCHANT_PARENT)
@Api(value = ApiConstants.MERCHANT_CONFIG_MANAGEMENT, tags = {
        ApiConstants.MERCHANT_CONFIG_MANAGEMENT
})
@SuppressWarnings("unchecked")
public class MerchantConfigController<T> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(MerchantConfigController.class);

    /** The merchant service. */
    @Autowired
    private MerchantConfigService merchantService;

    /** The response creator. */
    @Autowired
    private GeneralResponseCreator responseCreator;

    /** The message source. */
    @Autowired
    private MessageSource messageSource;

    /**
     * Adds the merchant config.
     *
     * @param model
     *            the model
     * @return the general response
     */
    @ApiOperation(value = "${swgr.op.merchant.add.value}", notes = "${swgr.op.merchant.add.note}")
    @PostMapping(path = ApiConstants.MERCHANT_ADD_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<T> addMerchantConfig(
            @ApiParam(required = true, value = "${swgr.an.merchant.update.value}") @RequestBody MerchantConfigDto model) {
        LOGGER.info("-------MerchantConfigController /addMerchantConfig()--------- Started");
        MerchantConfigDto merchantDto = merchantService.addMerchantConfig(model);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) merchantDto);
    }

    /**
     * Update merchant config.
     *
     * @param merchantId
     *            the merchant id
     * @param model
     *            the model
     * @return the general response
     */
    @PostMapping(path = ApiConstants.MERCHANT_UPDATE_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<T> updateMerchantConfig(@ApiParam(value = "${swagger.param.merchantId}", required = true) @RequestParam String merchantId,
            @Validated @RequestBody MerchantConfigDto model) {
        LOGGER.info("-------MerchantConfigController /updateMerchantConfig()--------- Started");
        MerchantConfigDto merchant = merchantService.getMerchantConfigById(merchantId);
        if (merchant == null) {
            throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_MERCHANT_ID, new Object[] {
                    merchantId
            }, Locale.getDefault()));
        }
        MerchantConfigDto updatedMerchant = merchantService.updateMerchantConfig(model);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) updatedMerchant);
    }

    /**
     * Gets the merchants list.
     *
     * @return the merchants list
     */
    @GetMapping(path = ApiConstants.MERCHANT_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<T> getMerchantsList() {
        LOGGER.info("-------MerchantConfigController /getMerchantsList()--------- Started");
        List<MerchantConfigDto> merchants = merchantService.getMerchantConfigList();
        LOGGER.info("-------MerchantConfigController /getMerchantsList()--------- end ");
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) merchants);
    }

    /**
     * Gets the merchant by id.
     *
     * @param merchantId
     *            the merchant id
     * @return the merchant by id
     */
    @GetMapping(path = ApiConstants.MERCHANT_LIST_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<T> getMerchantById(@ApiParam(required = true, value = "${swagger.param.merchantId}") @RequestParam String merchantId) {
        LOGGER.info("-------MerchantConfigController /getMerchantById()--------- started");
        MerchantConfigDto dto = merchantService.getMerchantConfigById(merchantId);
        if (dto == null) {
            throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_MERCHANT_ID, new Object[] {
                    merchantId
            }, Locale.getDefault()));
        }

        LOGGER.info("-------MerchantConfigController /getMerchantById()--------- end ");
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) dto);
    }

    /**
     * Delete merchant.
     *
     * @param merchantId
     *            the merchant id
     * @return the general response
     */
    @DeleteMapping(path = ApiConstants.MERCHANT_DELETE_CONFIG, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<T> deleteMerchant(@ApiParam(value = "${swagger.param.merchantId}", required = true) @RequestParam String merchantId) {
        LOGGER.info("-------MerchantConfigController /deleteMerchant()--------- started");
        MerchantConfigDto dto = merchantService.getMerchantConfigById(merchantId);
        if (dto == null) {
            throw new EntityNotFoundException(messageSource.getMessage(ApiConstants.ERROR_INVALID_MERCHANT_ID, new Object[] {
                    merchantId
            }, Locale.getDefault()));
        }
        merchantService.deleteMerchantConfig(dto);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) dto);
    }

}
