package com.tarang.middleware.configserver.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.TargetResponseDto;
import com.tarang.middleware.configserver.service.TargetResponseService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class TargetResponsesController.
 * 
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.TARGET_RESPONSE_PARENT)
@Api(value = ApiConstants.TARGET_RESPONSE_MANAGEMENT, tags = {
		ApiConstants.TARGET_RESPONSE_MANAGEMENT
})
public class TargetResponseController {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TargetResponseController.class);

	/** The target service. */
	@Autowired
	private TargetResponseService targetService;

	/** The response creator. */
	@Autowired
	private GeneralResponseCreator responseCreator;

	/** The message source. */

	@Autowired
	private MessageSource messageSource;

	/**
	 * Add target responses.
	 *
	 * @param targetmodel
	 *            the targetmodel
	 * @return the general response
	 */
	@ApiOperation(value = "${swgr.op.target.add.value}", notes = "${swgr.op.target.add.note}")
	@PostMapping(path = ApiConstants.TARGET_RESPONSE_ADD_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<TargetResponseDto> addTargetResponses(
			@ApiParam(required = true, value = "${swgr.an.target.add.value}") @RequestBody TargetResponseDto targetModel) {
		LOGGER.info("------TargetResponsesController /addTargetResponses()--------- Started");
		TargetResponseDto responsesDto = targetService.addTargetResponses(targetModel);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, responsesDto);

	}

	/**
	 * Update target responses.
	 *
	 * @param targetId
	 *            the target id
	 * @param targetresponsesmodel
	 *            the target responses model
	 * @return the general response
	 */
	@PostMapping(path = ApiConstants.UPDATE_TARGET_RESPONSE ,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<TargetResponseDto> updatetargetresponses(
			@ApiParam(value = "${swagger.param.targetId}", required = true) @RequestParam String targetId,
			@Validated @RequestBody TargetResponseDto targetResponsesModel) {
		LOGGER.info("------TargetResponsesController /updateTargetResponses()--------- Started");
		TargetResponseDto target = targetService.getTargetById(targetId);
		if (target == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetId", new Object[] {
					Integer.valueOf(targetId)
			}, Locale.getDefault()));
		}
		TargetResponseDto updatetargetDto = targetService.updateTargetResponses(targetResponsesModel);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, updatetargetDto);
	}

	/**
	 * Gets the targetresponseslist.
	 *
	 * @return the targetresponseslist
	 */
	@GetMapping(path = ApiConstants.TARGET_RESPONSE_LIST,  produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<HashMap<String,List<TargetResponseDto>>> getTargetResponsesList() {
		LOGGER.info("-------TargetResponsesController /getTargetResponsesList()--------- Started");
		HashMap<String,List<TargetResponseDto>> response = targetService.getSourceResponsesList();
		LOGGER.info("-------TargetResponsesController /getTargetResponsesList()--------- end ");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, response);
	}

	/**
	 * Gets the target responses by id.
	 *
	 * @param targetId
	 *            the target id
	 * @return the target responses by id
	 */
	@GetMapping(path = ApiConstants.TARGET_RESPONSE_LIST_BY_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<List<TargetResponseDto>> getTargetResponsesbyId(
			@ApiParam(value = "${swagger.param.targetid}", required = true) @RequestParam String targetId,String languageCode) {
		LOGGER.info("------TargetResponsesController /getTargetResponsesbyid()--------- Started");
		List<TargetResponseDto> dto = targetService.getTargetIdAndLanguageCode(targetId,languageCode);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetId", new Object[] {
					targetId
			}, Locale.getDefault()));
		}
		LOGGER.info("------TargetResponsesController /getTargetResponsebyid()--------- end");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, dto);

	}

	/**
	 * Delete target responses.
	 *
	 * @param targetId
	 *            the target id
	 * @return the general response
	 */
	@DeleteMapping(path = ApiConstants.TARGET_RESPONSE_DELETE ,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public GeneralResponse<TargetResponseDto> deleteTargetResponses(
			@ApiParam(value = "${swagger.param.targetid}", required = true) @RequestParam String targetId) {
		LOGGER.info("------SourceResponsesController /deleteSourceResponse()--------- Started");
		TargetResponseDto dto = targetService.getTargetById(targetId);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetid", new Object[] {
					targetId
			}, Locale.getDefault()));
		}
		targetService.deleteTargetResponses(dto);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, dto);
	}
}
