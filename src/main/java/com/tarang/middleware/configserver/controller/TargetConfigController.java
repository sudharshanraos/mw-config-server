package com.tarang.middleware.configserver.controller;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.TargetConfigDetailsDto;
import com.tarang.middleware.configserver.dto.TargetConfigDto;
import com.tarang.middleware.configserver.service.TargetConfigService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class TargetConfigController.
 *
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.TARGET_PARENT)
@Api(value = ApiConstants.TARGET_CONFIG_MANAGEMENT, tags = {
		ApiConstants.TARGET_CONFIG_MANAGEMENT
})
@SuppressWarnings("unchecked")
public class TargetConfigController<T> {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TargetConfigController.class);

	/** The target service. */
	@Autowired
	private TargetConfigService targetService;

	/** The response creator. */
	@Autowired
	private GeneralResponseCreator responseCreator;

	/** The message source. */
	@Autowired
	private MessageSource messageSource;

	/**
	 * Adds the target config.
	 *
	 * @param model
	 *            the model
	 * @return the general response
	 */
	@ApiOperation(value = "${swgr.op.merchant.add.value}", notes = "${swgr.op.merchant.add.note}")
	@PostMapping(path = ApiConstants.TARGET_ADD_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> addTargetConfig(
			@ApiParam(required = true, value = "${swgr.an.merchant.update.value}") @RequestBody TargetConfigDto model) {

		LOGGER.info("-------TargetConfigController /addTargetConfig()--------- Started");
		TargetConfigDto targetDto = targetService.addTargetConfig(model);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) targetDto);
	}

	/**
	 * Update target config.
	 *
	 * @param targetName
	 *            the target name
	 * @param model
	 *            the model
	 * @return the general response
	 */
	@PostMapping(path = ApiConstants.TARGET_UPDATE_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> updateTargetConfig(@ApiParam(value = "${swagger.param.merchantId}", required = true) @RequestParam String targetName,
			@Validated @RequestBody TargetConfigDto model) {
		LOGGER.info("-------TargetConfigController /updateTargetConfig()--------- Started");

		TargetConfigDto target = targetService.getTargetConfigByName(targetName);
		if (target == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetName", new Object[] {
					targetName
			}, Locale.getDefault()));
		}

		TargetConfigDto updatedTarget = targetService.updateTargetConfig(model);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) updatedTarget);

	}

	/**
	 * Gets the target config list.
	 *
	 * @return the target config list
	 */
	@GetMapping(path = ApiConstants.TARGET_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> getTargetConfigList() {
		LOGGER.info("-------TargetConfigController /getTargetConfigList()--------- Started");
		List<TargetConfigDto> target = targetService.getTargetConfigList();
		LOGGER.info("-------TargetConfigController /getTargetConfigList()--------- end ");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) target);
	}

	/**
	 * Gets the target config by name.
	 *
	 * @param targetName
	 *            the target name
	 * @return the target config by name
	 */
	@GetMapping(path = ApiConstants.TARGET_LIST_BY_TARGET_NAME, produces = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> getTargetConfigByName(
			@ApiParam(required = true, value = "${swagger.param.merchantId}") @RequestParam String targetName) {
		LOGGER.info("-------TargetConfigController /getMerchantById()--------- started");
		TargetConfigDto dto = targetService.getTargetConfigByName(targetName);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.targetName", new Object[] {
					targetName
			}, Locale.getDefault()));
		}

		LOGGER.info("-------TargetConfigController /getMerchantById()--------- end ");
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) dto);

	}

	/**
	 * Delete target config.
	 *
	 * @param targetName
	 *            the target name
	 * @return the general response
	 */
	@DeleteMapping(path = ApiConstants.TARGET_DELETE_CONFIG, produces = MediaType.APPLICATION_JSON_VALUE)
	public GeneralResponse<T> deleteTargetConfig(@ApiParam(value = "${swagger.param.merchantId}", required = true) @RequestParam String targetName) {
		LOGGER.info("-------TargetConfigController /deleteTargetConfig()--------- started");
		TargetConfigDto dto = targetService.getTargetConfigByName(targetName);
		if (dto == null) {
			throw new EntityNotFoundException(messageSource.getMessage("error.invalid.merchantId", new Object[] {
					targetName
			}, Locale.getDefault()));
		}
		targetService.deleteTargetConfig(dto);
		return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, (T) dto);
	}



	@GetMapping(path=ApiConstants.GET_TARGET_CONFIG,produces=MediaType.APPLICATION_JSON_VALUE)
	public TargetConfigDetailsDto getAllTargetConfig(@ApiParam(value="${swqger.param.port}",required=true)@RequestParam String port){
		LOGGER.info("----TargetConfigController / getAllTargetConfig()--------Started");
		TargetConfigDetailsDto dto=targetService.getAllTargetConfig(port);
		LOGGER.info("----TargetConfigController / getAllTargetConfig()-------end");
		return dto;
	}
	
	@PostMapping(path=ApiConstants.SAVE_TARGET_DETAILS,consumes=MediaType.APPLICATION_JSON_VALUE)
	public TargetConfigDetailsDto saveTargetDetails(@ApiParam(required = true, value = "${swgr.an.merchant.update.value}") @RequestBody TargetConfigDetailsDto model) {
		LOGGER.info("-------TargetConfigController / saveTargetDetails()-------Started");
		TargetConfigDetailsDto dto =targetService.saveTargetConfigDetails(model);
		LOGGER.info("-------TargetConfigController / saveTargetDetails()-------end");
		return dto;

	}
}
