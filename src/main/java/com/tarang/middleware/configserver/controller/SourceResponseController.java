package com.tarang.middleware.configserver.controller;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tarang.middleware.configserver.constants.ApiConstants;
import com.tarang.middleware.configserver.constants.ConfigServerErrorCodes;
import com.tarang.middleware.configserver.dto.SourceResponseDto;
import com.tarang.middleware.configserver.service.SourceResponseService;
import com.tarang.middleware.configserver.utils.GeneralResponse;
import com.tarang.middleware.configserver.utils.GeneralResponseCreator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * The Class SourceResponsesController.
 * 
 * @author sudharshan.s
 */
@RestController
@RequestMapping(ApiConstants.SOURCE_RESPONSE_PARENT)
@Api(value = ApiConstants.SOURCE_RESPONSE_MANAGEMENT, tags = {
        ApiConstants.SOURCE_RESPONSE_MANAGEMENT
})
public class SourceResponseController {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(SourceResponseController.class);

    /** The response service. */
    @Autowired
    private SourceResponseService responseService;

    /** The response creator. */
    @Autowired
    private GeneralResponseCreator responseCreator;

    /** The message source. */
    @Autowired
    private MessageSource messageSource;

    /**
     * Add source response.
     *
     * @param responsemodel
     *            the response model
     * @return the general response
     */
    @ApiOperation(value = "${swgr.op.source.add.value}", notes = "${swgr.op.source.add.note}")
    @PostMapping(path = ApiConstants.SOURCE_RESPONSE_ADD_CONFIG, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<SourceResponseDto> addSourceResponse(
            @ApiParam(required = true, value = "${swgr.an.source.add.value}") @RequestBody SourceResponseDto responseModel) {
        LOGGER.info("------SourceResponsesController /addSourceResponse()--------- Started");
        SourceResponseDto resourceDto = responseService.addSourceResponse(responseModel);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, resourceDto);
    }

    /**
     * Update source response.
     *
     * @param sourceId
     *            the source id
     * @param resourceModel
     *            the resource model
     * @return the general response
     */
    @PatchMapping(path = ApiConstants.UPDATE_SOURCE_RESPONSE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<SourceResponseDto> updateSourceResponse(
            @ApiParam(value = "${swagger.param.sourceId}", required = true) @RequestParam String sourceId,
            @Validated @RequestBody SourceResponseDto resourceModel) {
        LOGGER.info("------SourceResponsesController /updateSourceResponse()--------- Started");
        SourceResponseDto responseEntity = responseService.getSourceById(sourceId);
        if (responseEntity == null) {
            throw new EntityNotFoundException(messageSource.getMessage("error.invalid.SourceId", new Object[] {
                    String.valueOf(sourceId)
            }, Locale.getDefault()));
        }
        SourceResponseDto updateResource = responseService.updateSourceResponse(resourceModel);
      return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, updateResource);
    }

    /**
     * Gets the source response list.
     *
     * @return the source response list
     */
    @GetMapping(path = ApiConstants.SOURCE_RESPONSE_LIST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<List<SourceResponseDto>> getSourceResponseList() {
        LOGGER.info("-------SourceResponsesController /getsourceresponseList()--------- Started");
        List<SourceResponseDto> response = responseService.getSourceResponseList();
        LOGGER.info("-------SourceResponseController /getSourceResponList()--------- end ");
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, response);
    }

    /**
     * Gets the source response by id.
     *
     * @param sourceId
     *            the source id
     * @return the source response by id
     */
    @GetMapping(path = ApiConstants.SOURCE_RESPONSE_LIST_BY_ID,  produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<List<SourceResponseDto>> getResponseById(
            @ApiParam(value = "${swagger.param.SourceId,languageCode}", required = true) @RequestParam String sourceId,String languageCode) {
    	LOGGER.info("------SourceResponsesController /getResponsebyid()--------- Started");
        List<SourceResponseDto> dto = responseService.getSourceIdAndLanguageCode(sourceId, languageCode);
        if (dto == null) {
            throw new EntityNotFoundException(messageSource.getMessage("error.invalid.Sourceid", new Object[] {
                    sourceId
            }, Locale.getDefault()));
        }
        LOGGER.info("------SourceResponsesController /getResponsebyid()--------- end");
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, dto);
    }

    /**
     * Delete source response.
     *
     * @param SourceId
     *            the sourceid
     * @return the general response
     */
    @DeleteMapping(path = ApiConstants.SOURCE_RESPONSE_DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public GeneralResponse<SourceResponseDto> deleteSourceResponse(
            @ApiParam(value = "${swagger.param.SourceId}", required = true) @RequestParam String sourceId) {
        LOGGER.info("------SourceResponsesController /deleteSourceResponse()--------- Started");
        SourceResponseDto dto = responseService.getSourceById(sourceId);
        if (dto == null) {
            throw new EntityNotFoundException(messageSource.getMessage("error.invalid.Sourceid", new Object[] {
                    sourceId
            }, Locale.getDefault()));
        }
 
        responseService.deleteSourceResponse(dto);
        return responseCreator.createGeneralResponse(ConfigServerErrorCodes.SUCCESS, true, dto);
    }

}
